package stockMarketGame.stockMarket.gameLogic.accountManager;
import java.util.Random;
import java.util.TimerTask;

import stockMarketGame.stockMarket.gameLogic.assets.ShareItem;
import stockMarketGame.stockMarket.gameLogic.stockMarket.*;

/**
 * @author David Yesil
 */
public class ProfitAgent extends Agent {

	/**
	 * Contructor of the Agent class
	 * @param acc AccountManager reference which the agent manipulates
	 * @param player Player object which gets manipulated by the agent
	 */
	public ProfitAgent(AccountManager acc, Player player)throws RuntimeException{
		super(acc, player);
	}

	@Override
	public void run() {		
		Random rng = new Random();
		ShareItem[] shareItems = this.player.getShareDepositAccount().getShareItems();
		for(ShareItem shareItem: shareItems){
			if(this.acc.isShareMoreValuable(this.player.getName(),  shareItem.getShare().getName())){
				this.acc.sellShare(this.player.getName(), shareItem.getShare().getName(), shareItem.getAmount());
			}
		}		
		Share [] shareSnapshot = this.acc.getStockPriceProvider().getAllSharesAsSnapshot();
		for(int i=0;i<2;i++){
			Share rndShare = shareSnapshot[rng.nextInt(shareSnapshot.length)];
			int rndInt = rng.nextInt(4)+1;
			if((rndShare.getSharePrice()*(rndInt))<=this.player.getCashAccount().getValue()){ //&rndShare.getSharePrice()<30
				this.acc.buyShare(this.player.getName(), rndShare.getName(), (rndInt));
			}			
		}
	}	
}