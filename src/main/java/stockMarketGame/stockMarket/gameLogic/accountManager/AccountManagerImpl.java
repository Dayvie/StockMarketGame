package stockMarketGame.stockMarket.gameLogic.accountManager;		
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TimerTask;
import java.util.TreeSet;

import stockMarketGame.stockMarket.gameLogic.assets.*;
import stockMarketGame.stockMarket.gameLogic.exceptions.*;
import stockMarketGame.stockMarket.gameLogic.stockMarket.*;
/***
 * This class holds the methods which can be used by the players.
 * It also contains the Player objects. It has a reference to the stockPriceProvider which manages the shares in the stockmarket
 * and the AssetViewer which is a gui which displays the stats of the players.
 * @author David Yesil
 */
public class AccountManagerImpl implements AccountManager {

	private Set<Player> players;
	private StockPriceProvider stockPriceProvider;	
	private AssetViewer assetViewer;
	/**
	 * @param stockPriceProvider the stockpriceprovider which will be used for managing the shares which will be offered.
	 * @throws NullParameterException if the parameter stockPriceProvider is null. 
	 * 
	 */ 
	public AccountManagerImpl(StockPriceProvider stockPriceProvider)throws NullParameterException{		
		if(stockPriceProvider == null){
			throw new NullParameterException();
		}
		this.stockPriceProvider = stockPriceProvider;
		this.assetViewer = new AssetViewer(this);
		this.players = new TreeSet<>();
		this.overrideDefaultLocale();	
	}
	
	/**
	 * Overwrites the defaultLocale based on the content of i18n/language. If uses en_GB as default language if no language is set.
	 */
	private void overrideDefaultLocale(){
		BufferedReader  br = new BufferedReader(new InputStreamReader(ClassLoader.getSystemResourceAsStream(
				"i18n/language.properties")));
		try {
			String readLine = br.readLine();
			br.close();
			if (readLine.equals("language=")){
				Locale.setDefault(new Locale("en", "GB"));
				return;
			}
			String language = readLine.split("=")[1].split("_")[0];
			String country  = readLine.split("=")[1].split("_")[1];
			Locale locale = new Locale(language, country);
			Locale.setDefault(locale);
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
	
	@Override
	public StockPriceProvider getStockPriceProvider(){
		return this.stockPriceProvider;
	}
	
	public AssetViewer getAssetViewer(){
		return this.assetViewer;
	}
	
	@Override
	public Player[] getPlayers(){
		return this.players.toArray(new Player[0]);
	}
	
	/**
	 * @param playerName The name of the Player Object in the players treeset which will be returned.
	 * @return returns the player with the fitting name.
	 * @throws PlayerNotFoundException
	 */
	private Player searchPlayer(String playerName)throws PlayerNotFoundException{
		try{
			return players.stream()
					.filter(player -> player.getName().equals(playerName))
					.findFirst().get();
		}catch(NullPointerException n){
			throw new PlayerNotFoundException();
		}
	}
	
	/**
	 * @param playerName Creates a player object with the String, set by the parameter, as its name.
	 * @throws PlayerAlreadyExistsException if a player with this name was already stored in players
	 */
	@Override
	public void createPlayer(String playerName)throws PlayerAlreadyExistsException{		
		this.players.forEach(player ->{
			if(playerName.equals(player.getName())){
				throw new PlayerAlreadyExistsException(); // player already in array stop				 				
			}
		});
		this.players.add(new Player(playerName));
	}	
	
	/**
	 * @author David Yesil
	 * @param playerName name of the player who buys shares
	 * @param shareName name of the shares which are bought
	 * @param amount of the shares which are bought
	 * @throws NotEnoughMoneyException if the player who bought a share hasn't enough money
	 * @throws ShareNotFoundException if the share which is going to be bought doesn't exist
	 * @throws PlayerNotFoundException if the player who buys the share doesn't exist
	 */
	@Override
	public void buyShare(String playerName, String shareName, int amount) throws NotEnoughMoneyException, ShareNotFoundException, PlayerNotFoundException {
		ResourceBundle resources = ResourceBundle.getBundle("i18n/language");
		Player player = this.searchPlayer(playerName);
		Share share = this.stockPriceProvider.searchShare(shareName);
		if((player.getCashAccount().getValue() - share.getSharePrice()*amount)<0){
			throw new NotEnoughMoneyException();					
		}
		player.getShareDepositAccount().buyShare(share, amount); //buyShare
		player.getCashAccount().withdraw(share.getSharePrice()*amount); //subtract value from CashAccount				
		player.addAccountStatement(player.getName()+" "+ resources.getString("bought")+" "+amount, new java.util.Date(), -share.getSharePrice()*amount, shareName);
	}
	
	/**
	 * @author David Yesil
	 * @param playerName name of the player who sells shares
	 * @param shareName name of the shares which are sold
	 * @param amount of the shares which are sold
	 * @throws ShareNotFoundException if the share which is going to be sold doesn't exist
	 * @throws PlayerNotFoundException if the player who will sell the share doesn't exist
	 * 
	 */
	@Override
	public void sellShare(String playerName, String shareName, int amount)throws ShareNotFoundException, PlayerNotFoundException {
		ResourceBundle resources = ResourceBundle.getBundle("i18n/language");
		Player player = this.searchPlayer(playerName);
		Share share = this.stockPriceProvider.searchShare(shareName);
		player.getShareDepositAccount().sellShare(share, amount);//sellShare
		player.getCashAccount().deposit(share.getSharePrice()*amount);//add value to CashAccount
		player.addAccountStatement(player.getName()+" "+ resources.getString("sold")+ " "+amount, new java.util.Date(), share.getSharePrice()*amount, shareName);
	}
	
	/**
	 * Prints accountStatements which are sorted by certain sortingcriterias (s(share), t(time), ShareName. The Statements can be saved in a HTML File(f)
	 * or displayed to the screen(scr)
	 */
	@Override
	public String printAccountStatements(String playerName, String sortingCriteria, MimeType mimeType){		
		Player player = this.searchPlayer(playerName);					
		AccountStatement[] playerAccountStatements = new AccountStatement[0];
		switch(sortingCriteria){
		case "Time":
			playerAccountStatements = player.getAccountStatementsSortedByTime();	
			break;
		case "Share":
			playerAccountStatements = player.getAccountStatementsSortedByShare();
			break;
		default:
			Share[] shares = this.stockPriceProvider.getAllSharesAsSnapshot();
			for(Share share:shares){
				if(sortingCriteria.equals(share.getName())){
					playerAccountStatements = player.getAccountStatementsOnlyOneShare(share);
				}				
			}	
			break;
		}
		switch(mimeType){
		case FILE:
			this.writeAccountStatementsToHTML(playerAccountStatements, playerName);
			return null;		
		case SCREEN:			
			StringBuilder sbuilder = new StringBuilder();
			for(AccountStatement accountStatement:playerAccountStatements){
			sbuilder.append(accountStatement.toString());
			}				
			return sbuilder.toString();
		}
		return null;
	}
	/**
	 * Help-method for printAccountStatements which writes AccountStatements to HTML-Files
	 * @param playerAccountStatements AccountStatements of a player in an array
	 * @param playerName Name of the player which gets his accountstatements printed
	 */
	private void writeAccountStatementsToHTML(AccountStatement[] playerAccountStatements, String playerName){	
		String username = System.getProperty("user.name");
		File accountFile = new File("C:/Users/"+username+"/Desktop/BoersenSpiel/AccountStatements/");
		try{ 	
			accountFile.mkdirs();
			accountFile = new File("C:/Users/"+username+"/Desktop/BoersenSpiel/AccountStatements/Accountstatements_"+playerName+".html");
			if (!accountFile.exists()){
				accountFile.createNewFile();
			}	
			ResourceBundle resources = ResourceBundle.getBundle("i18n/language");
			SimpleDateFormat sdf = new SimpleDateFormat(resources.getString("dateformat"));
			BufferedWriter accountFileWriter = new BufferedWriter(new FileWriter(accountFile));
			accountFileWriter.write("<!doctype html><html><head><meta charset=\"utf-8\">"
					+ "<title>Accountstatements</title><style>table, td, th { border: 1px solid black; }</style>"
					+ "</head> <body> <h1>"+resources.getString("accountstatements")+"</h1> <table><tr><th>"+resources.getString("transactiondate")+"</th><th>"+resources.getString("description")+"</th><th>"+resources.getString("changeInValue")+"</th></tr>");
			for(AccountStatement accountStatement: playerAccountStatements){
				accountFileWriter.write("<tr><td>" + sdf.format(accountStatement.getDate()).toString()+ "</td> "
						+ "<td>"+accountStatement.getDescription()+" "+accountStatement.getShareName()+"</td><td>" +accountStatement.getValue()+ ""
								+ "</td></tr>");
			}				
			accountFileWriter.write("</table></body></html>");
			accountFileWriter.close();	
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Returns a String with the content of a player's sharedepositaccount
	 * @param playerName Name of the player which gets his shareDepositAccount written to a String
	 * 
	 */
	@Override
	public String shareDepositAccountToString(String playerName){
		Player player = this.searchPlayer(playerName);		
		return player.getShareDepositAccount().toString();
	}
	
	/**
	 * Is used to update the values which the player objects hold
	 */
	@Override
	public void updatePlayers(){
		this.players.forEach(player -> player.getShareDepositAccount().update());
	}
	
	@Override
	public long getAssetValue(String playerName, String asset) throws AssetNotFoundException {		
		Player player = this.searchPlayer(playerName);
		if(asset == "SDP"){			
			return player.getShareDepositAccount().getValue();			
		}
		if(asset == "CA"){			
			return player.getCashAccount().getValue();	
		}
		else{
			return Arrays.asList(player.getShareDepositAccount().getShareItems())
			.parallelStream().filter(shareItem -> shareItem.getName().equals(asset))
			.map(shareItem -> shareItem.getValue()).findFirst().get();
		}
	}
	
	/**
	 * Calculates the Total value of a players assets
	 * @param playerName name of the player which gets his total value calculated
	 */
	@Override
	public long getAssetTotal(String playerName) throws PlayerNotFoundException{		
		Player player = this.searchPlayer(playerName);
		player.getShareDepositAccount().update();
		return (player.getCashAccount().getValue()+player.getShareDepositAccount().getValue());		
	}	
	/**
	 * prints all players and their assets
	 */
	public String playersToString(){
		StringBuilder sBuilder = new StringBuilder();
		players.stream().forEach(player -> sBuilder.append(" | "+player.toString()+" |\n"));
		return sBuilder.toString();
	}
	/**
	 * 		
	 * @param playerName the name of the player whose informations will be returned
	 * @return the informations which a player holds in one String
	 */
	@Override
	public String playerToString(String playerName){
		return searchPlayer(playerName).toString();
	}
	/**
	 * starts the timer of the StockPriceViewer
	 */
	@Override
	public void startUpdate(){
		this.stockPriceProvider.startUpdate();
	}
	/**
	 * @return returns true if selling the share would be profitable, else false
	 * @param playerName The name of the player which wants to see if selling a share would be profitable
	 * @param shareName The name of the shareitem in the sharedepositaccount of a player which will be compared to the share in the stockmarket
	 */
	@Override
	public boolean isShareMoreValuable(String playerName, String shareName) {
		Player player = this.searchPlayer(playerName);
		long ownedShare = 0;
		long marketShare = 0;
		ShareItem[] shareItems = player.getShareDepositAccount().getShareItems();
		for(ShareItem shareItem : shareItems){
			if(shareItem.getName().equals(shareName)){
				ownedShare = shareItem.getShare().getSharePrice();
			}
		}
		marketShare = Arrays.asList(this.stockPriceProvider.getAllSharesAsSnapshot())
				.stream().filter(share -> share.getName().equals(shareName))
				.findFirst().get().getSharePrice();
		if(ownedShare == 0){
			return false;
		}
		long profit = marketShare - ownedShare;
		if(profit >0){
			return true;
		}
		else{
			return false;
		}
	}
	
	@Override
	public void createRngAgent(String playerName)throws RuntimeException{
		Player player = this.searchPlayer(playerName);
		new RandomAgent(this, player).start();
	}

	@Override
	public void createProfitAgent(String playerName)throws RuntimeException {
		Player player = this.searchPlayer(playerName);
		new ProfitAgent(this, player).start();
	}
	
	@Override
	public void deleteAgent(String playerName)throws RuntimeException{
		Player player = this.searchPlayer(playerName);
		player.turnAgentOff();
	}
}