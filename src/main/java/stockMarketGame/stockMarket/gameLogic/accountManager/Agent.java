package stockMarketGame.stockMarket.gameLogic.accountManager;

import java.util.TimerTask;

import stockMarketGame.stockMarket.gameLogic.stockMarket.Ticker;

public abstract class Agent implements Runnable {	
	protected AccountManager acc;
	protected Player player;
	protected TimerTask task;
	
	public Agent(AccountManager acc, Player player)throws RuntimeException{
		if(player.getAgent() != null){
			throw new RuntimeException("Player has already an Agent");
		}else{
			this.acc = acc;
			this.player = player;
			player.setAgent(this);
		}
	}	
	
	public void stop(){
		task.cancel();
	}
	
	public void start(){
		Ticker  singleTicker = Ticker.getInstance();
		task = new TimerTask() {//anonymous class
	    	@Override
			public void run() {
	            Agent.this.run(); //buy or sell rnd shares     
	    	}
		};
		singleTicker.scheduleAtFixedRate(task, 0, 500);	
	}
}