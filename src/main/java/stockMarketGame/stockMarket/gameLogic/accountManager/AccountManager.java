package stockMarketGame.stockMarket.gameLogic.accountManager;
import javafx.scene.Node;
import javafx.scene.control.Label;
import stockMarketGame.stockMarket.commandShell.AsCommand;
import stockMarketGame.stockMarket.gameLogic.assets.AssetViewer;
import stockMarketGame.stockMarket.gameLogic.exceptions.NotEnoughMoneyException;
import stockMarketGame.stockMarket.gameLogic.exceptions.PlayerNotFoundException;
import stockMarketGame.stockMarket.gameLogic.exceptions.ShareNotFoundException;
import stockMarketGame.stockMarket.gameLogic.stockMarket.StockPriceProvider;
/*
 * @AsCommand for Shell
 */
public interface AccountManager {	
	@AsCommand(command = "crp", description ="*creates a player; ", paramTypes= String.class)
	public void createPlayer(String playerName);	
	@AsCommand(command = "bs", description ="*buys a share; ", paramTypes={String.class, String.class, int.class})
	public void buyShare(String playerName, String shareName, int amount)throws NotEnoughMoneyException, ShareNotFoundException, PlayerNotFoundException;	
	@AsCommand(command = "ss", description ="*sells a share; ", paramTypes={String.class, String.class, int.class})
	public void sellShare(String playerName, String shareName, int amount);
	public long getAssetValue(String playerName, String assetName);
	public long getAssetTotal(String playerName);
	@AsCommand(command = "assets", description ="*shows assets of the player ", paramTypes={String.class})
	String playerToString(String playerName);
	String playersToString(); 
	StockPriceProvider getStockPriceProvider();
	String shareDepositAccountToString(String playerName);
	Player[] getPlayers();
	public void updatePlayers();
	@AsCommand(command = "cmp", description ="*checks if selling this asset would lead to\n"
			+ "financial gain ", paramTypes={String.class, String.class})
	public boolean isShareMoreValuable(String playerName, String shareName);	
	@AsCommand(command = "start", description ="*starts the ticker; ", paramTypes = {})
	public void startUpdate();
	@AsCommand(command = "rndag", description ="*creates an agent who buys/sells shares randomly ", paramTypes={String.class})
	public void createRngAgent(String playerName)throws RuntimeException;
	@AsCommand(command = "proag", description = "*creates an agent which sells only\n"
			+ "shares if it makes profit ", paramTypes={String.class})
	public void createProfitAgent(String playername)throws RuntimeException;
	@AsCommand(command = "pas", description = "*prints accountstatements which are sorted\n"
			+ "by certain inputcriteria: t(sorted by time),\n"
			+ "s(sorted by sharetype), <sharename>\n"
			+ "(only this share, sorted by time);\n"
			+ "mime parameter for the \n"
			+ "output destination: scr (screen), f(file) ",
			paramTypes={String.class, String.class, String.class})
	String printAccountStatements(String playerName, String sortingCriteria, MimeType mimeType);	
	public Label getAssetViewer();
	@AsCommand(command = "delAg", description = "deletes an agent of a player", paramTypes={String.class})
	public void deleteAgent(String playerName);
}