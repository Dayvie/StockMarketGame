package stockMarketGame.stockMarket.gameLogic.accountManager;

public enum MimeType {
	SCREEN("Screen"), FILE("File");
	
	public String name;
	
	MimeType(String name){
		this.name = name;
	}
}

