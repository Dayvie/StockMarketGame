package stockMarketGame.stockMarket.gameLogic.accountManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

import stockMarketGame.stockMarket.gameLogic.assets.CashAccount;
import stockMarketGame.stockMarket.gameLogic.assets.ShareDepositAccount;
import stockMarketGame.stockMarket.gameLogic.exceptions.*;
import stockMarketGame.stockMarket.gameLogic.stockMarket.Share;
/**
 * 
 * @author David Yesil
 * Generates objects which represents a player. Holds a LinkedList  which contains the accountStatements of the player,
 * The name of the player, the shareDepositAccount of the player and, his cashaccount.
 *
 */
public class Player implements Comparable<Player>{

	private List<AccountStatement> accountStatements; 
	private String playerName;
	private ShareDepositAccount shareDepositAccount;
	private CashAccount cashAccount;
	private Agent agent;
	/**
	 * @param name of the player which should be constructed
	 * @throws NullParameterException if the input is null
	 */
	public Player(String name)throws NullParameterException{		
		if(name == null){
			throw new NullParameterException();
		}
		this.playerName = name;
		this.cashAccount = new CashAccount(name, 1000);
		this.shareDepositAccount = new ShareDepositAccount(this.playerName);
		this.accountStatements = new LinkedList<>();
	}
	
	public Agent getAgent(){
		return this.agent;
	}
	
	public void setAgent(Agent agent){
		this.agent = agent;
	}
	
	public void turnAgentOff(){
		this.agent.stop();
		this.agent = null;
	}
	/**
	 * 
	 * @param description description of the generated AccountStatement
	 * @param date date of the transaction
	 * @param value value of the transaction, values which decreased the currency on the cashaccount are negative
	 * @param shareName Name of the player
	 * 
	 * Method which generates an accountStatement: Generated accountstatements are added to the LinkedList of the player.
	 */
	public void addAccountStatement(String description, Date date, long value, String shareName){
		this.accountStatements.add(new AccountStatement(description, date, value, shareName));
	}
	/**
	 * 
	 * @return the accountstatements which were sorted by date in an array 
	 */
	public AccountStatement[] getAccountStatementsSortedByTime(){
		this.accountStatements.sort((s1, s2) -> {return s1.getDate().compareTo(s2.getDate());});
		return this.accountStatements.toArray(new AccountStatement[this.accountStatements.size()]);
	}
	/**
	 * 
	 * @return the accountstatements which were primarily sorted by sharename and secondarily sorted by date in an array
	 */
	public AccountStatement[] getAccountStatementsSortedByShare(){
		this.accountStatements.sort((s1, s2) -> {return s1.getDate().compareTo(s2.getDate());});
		this.accountStatements.sort((s1, s2) -> {return s1.getShareName().compareTo(s2.getShareName());});		
		return this.accountStatements.toArray(new AccountStatement[this.accountStatements.size()]);
	}
	/**
	 * 
	 * @param share the name of the share
	 * @return an array which was sorted by sharename and by date afterwards, of only one share
	 */
	public AccountStatement[] getAccountStatementsOnlyOneShare(Share share){
		this.accountStatements.sort((s1, s2) -> {return s1.getDate().compareTo(s2.getDate());});
		this.accountStatements.sort((s1, s2) -> {return s1.getShareName().compareTo(s2.getShareName());});
		ArrayList<AccountStatement> accountStatementArr = new ArrayList<>();
		for(AccountStatement statement:this.accountStatements){
			if(statement.getShareName().equals(share.getName())){
				accountStatementArr.add(statement);
			}
		}
		return accountStatementArr.toArray(new AccountStatement[accountStatementArr.size()]);
	}
	
	public ShareDepositAccount getShareDepositAccount(){	
		return this.shareDepositAccount;	
	}
	
	public String getName(){		
		return this.playerName;		
	}
	
	public CashAccount getCashAccount(){		
		return this.cashAccount;		
	}
	/**
	 * Returns a String which contains informations about the assets of a player
	 */
	@Override
	public String toString(){	
		ResourceBundle resources = ResourceBundle.getBundle("i18n/language");
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append(resources.getObject("playername")+": ")
		.append(this.playerName)
		.append("; "+resources.getObject("valueshareitem")+": ")		
		.append(this.shareDepositAccount.getValue())
		.append(" ")
		.append(resources.getString("currency"))
		.append(" ")
		.append("; "
				+ resources.getString("accountbalance")
				+ ": ")
		.append(this.cashAccount.getValue())	
		.append(" ")
		.append(resources.getString("currency"));
		return sBuilder.toString();				
	}
	/**
	 * Comparator which sorts Players by their names
	 */
	@Override
	public int compareTo(Player player) {
	    return this.playerName.compareTo(player.getName());
	}
}