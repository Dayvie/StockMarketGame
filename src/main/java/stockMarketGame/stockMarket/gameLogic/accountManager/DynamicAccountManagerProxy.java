package stockMarketGame.stockMarket.gameLogic.accountManager;

import java.io.File;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Logger;

/**
 * Proxy which receives methods which an AccountManager interface is able to receive.
 * Does log the methodnames, parameter and returnvalues of invoked methods.
 * Sends the received methods to an implementation of AccountManager.
 * @author David Yesil
 *
 */
public class DynamicAccountManagerProxy implements InvocationHandler{

	static{
		String username = System.getProperty("user.name");
		File file = new File("C:/Users/"+username+"/Desktop/BoersenSpiel/LoggingFrameWork/");
		file.mkdirs();
		System.setProperty("java.util.logging.config.file", "LogConfig/LogConfig.txt");
	}
	
	private static final Logger log = Logger.getLogger("FineLogger"); 
	private AccountManager acc;
	
	public DynamicAccountManagerProxy(AccountManager acc){
		this.acc = acc;	
	}
	
	@Override
	public Object invoke(Object proxy, Method method, Object[] args){
		Object result = null;
        try  {       	
        	log.fine("methodname: "+method.getName());
        	StringBuilder sbuilder = new StringBuilder();
        	sbuilder.append("parameter: ");
        	if(args != null){
        		for (Object arg : args) {
            		sbuilder.append(arg.toString()+" ");
            	}
        	}
        	String parameter = sbuilder.toString();
        	log.fine(parameter);
            result = method.invoke(this.acc, args);        	
        } catch(IllegalAccessException ex)  {        	
        	log.severe(ex.toString());
        	ex.printStackTrace();
        } catch(InvocationTargetException ex)  {
        	log.severe(ex.toString());
        	ex.printStackTrace();
        }
        log.fine("result: "+result);
        return result;
	}
}