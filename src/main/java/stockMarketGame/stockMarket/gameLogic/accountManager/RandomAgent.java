package stockMarketGame.stockMarket.gameLogic.accountManager;
import java.util.Random;
import java.util.TimerTask;

import stockMarketGame.stockMarket.gameLogic.assets.ShareItem;
import stockMarketGame.stockMarket.gameLogic.stockMarket.*;
public class RandomAgent extends Agent{
	
	/**
	 * Contructor of the Agent class
	 * @param acc AccountManager reference which the agent manipulates
	 * @param player Player object which gets manipulated by the agent
	 */
	public RandomAgent(AccountManager acc, Player player)throws RuntimeException{		
		super(acc, player);
	}

	@Override
	public void run(){
		ShareItem[] shareItems = this.player.getShareDepositAccount().getShareItems();
		Random rng = new Random();
		Share[] shares = this.acc.getStockPriceProvider().getAllSharesAsSnapshot();
		int rndInt = rng.nextInt(4)+1;
		Share rndShare = shares[rng.nextInt((shares.length))];
		if(rng.nextBoolean()){
			if((rndShare.getSharePrice()*rndInt)<this.player.getCashAccount().getValue()){
				this.acc.buyShare(this.player.getName(), rndShare.getName(), rndInt);
			}			
		}
		else{
			if(shareItems.length > 0){			
				ShareItem randomShareItem = shareItems[rng.nextInt(shareItems.length)];
				this.acc.sellShare(this.player.getName(), randomShareItem.getShare().getName(), rng.nextInt(randomShareItem.getAmount())+1);
			}
		}	
	}
}