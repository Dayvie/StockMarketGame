package stockMarketGame.stockMarket.gameLogic.accountManager;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

public class AccountStatement{
	/*
	 * Object which holds values
	 */
	private String description;
	private Date date;
	private long value;
	private String shareName;
	
	public AccountStatement(String description, Date date, long value, String shareName){
		this.description = description;
		this.date = date;
		this.value = value;
		this.shareName = shareName;	
	}

	public String getDescription() {
		return this.description;
	}

	public Date getDate() {
		return this.date;
	}

	public long getValue() {
		return this.value;
	}
	
	public String getShareName(){
		return this.shareName;
	}
	
	@Override
	public String toString(){
		ResourceBundle resources = ResourceBundle.getBundle("i18n/language");
		SimpleDateFormat sdf = new SimpleDateFormat(resources.getString("dateformat"));		
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append(sdf.format(this.date).toString())		
		.append(" ").append(this.description)		
		.append(" ").append(this.shareName)	
		.append(" ").append(this.value)
		.append(" ").append(resources.getObject("currency"))
		.append("\n");
		return sBuilder.toString();
	}
}