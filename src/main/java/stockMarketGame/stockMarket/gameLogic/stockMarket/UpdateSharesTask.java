package stockMarketGame.stockMarket.gameLogic.stockMarket;

import java.util.TimerTask;

public class UpdateSharesTask extends TimerTask {

	private StockPriceInfo stockPriceInfo;
	
	public UpdateSharesTask(StockPriceInfo stockPriceInfo){
		this.stockPriceInfo = stockPriceInfo;
	}
	
	@Override
	public void run() {
		this.stockPriceInfo.updateSharePrices();
	}	
}