package stockMarketGame.stockMarket.gameLogic.stockMarket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class HistoricalStockPriceProvider extends StockPriceProvider{
	private Map<String, Reader> readers;
	HashMap<String, String> urls = new HashMap<>();	
	
	public HistoricalStockPriceProvider(){
		super();
		this.readers = new HashMap<>();
		this.downloadAllStockFiles();				
	}
	/**
	 * @param share share which will be updated
	 * 
	 * Updates a share based on a historical shareprice which is read from a .csv file
	 */
	@Override
	public void updateSharePrice(Share share){			
		BufferedReader bufferedReader = (BufferedReader)this.readers.get(share.getName());
		try {
			String readLine = bufferedReader.readLine();
			if(readLine == null){//csv file has no content left
				this.readers.remove(share.getName());
				bufferedReader.close(); // last reference to the reader gets overwritten //skip first line
				bufferedReader = buildAndPutNewReader(share.getName());
				readLine = bufferedReader.readLine(); //create new reader and start from the beginning		
			}
			Random rng = new Random();
			share.setSharePrice((long)Double.parseDouble(readLine.split(",")[rng.nextInt(3)+1]));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private BufferedReader buildAndPutNewReader(String shareName){	
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(new URL(urls.get(shareName)).openStream()));
			in.readLine();
			readers.put(shareName, in);
			return in;
		} catch (IOException e) {
			e.printStackTrace();
		}	
		return null;
	}
	
	private void downloadAllStockFiles(){	
		urls.put("Adidas", "http://www.google.com/finance/historical?q=ETR%3AADS&ei=-7NhVamTEceY8gPJtYOoDA&output=csv"); 
		urls.put("BMW", "http://www.google.com/finance/historical?q=FRA%3ABMW&ei=rLRhVenjC8Gh8APtmIPgDQ&output=csv"); 
		urls.put("DeutscheBank", "http://www.google.com/finance/historical?q=FRA%3ADBK&ei=HrVhVYHeNISf8gOSlYOoDA&output=csv"); 
		urls.put("Siemens", "http://www.google.com/finance/historical?q=FRA%3ASIE&ei=jrZhVZnsA8zr8QOc8IPwBA&output=csv");
		urls.put("Lufthansa", "http://www.google.com/finance/historical?q=ETR%3ALHA&ei=2bZhVcGoMcGh8APtmIPgDQ&output=csv");
		urls.put("Daimler", "http://www.google.com/finance/historical?q=OTCMKTS%3ADDAIF&ei=jrdhVcj_IcLA8gPVuYO4DQ&output=csv");
		urls.put("DeutschePost", "http://www.google.com/finance/historical?q=ETR%3ADPW&ei=ybdhVenIFIXZ8gOXpoCoBA&output=csv");
		for(Share share:this.shares){
			try (	BufferedReader in = new BufferedReader(new InputStreamReader(new URL(urls.get(share.getName())).openStream()));){	
					in.readLine();
					readers.put(share.getName(), in);			
			} catch(MalformedURLException e) {
				e.printStackTrace();
			} catch(IOException e) {
				e.printStackTrace();
			}	
		}
	}
}