package stockMarketGame.stockMarket.gameLogic.stockMarket;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
/**
 * A Ticker (jdk class) which can only exists once per runtime.
 * @author David
 *
 */
public class Ticker extends Timer{    
    private static  Ticker firstInstance = null;	    
    private List<TimerTask> tasks;
    
    private Ticker(){
    	this.tasks = new ArrayList<>();
    }
    
    public static Ticker getInstance(){			
		if(firstInstance == null){			
			firstInstance = new Ticker();
		}		
		return firstInstance;		
	}	       	        
}
