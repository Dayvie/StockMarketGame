package stockMarketGame.stockMarket.gameLogic.stockMarket;
import java.util.Random;

public class RandomStockPriceProvider extends StockPriceProvider {

	@Override
	public void updateSharePrice(Share share) {	
    	Random rng = new Random();
    	long newRandomSharePrice;
    	if(rng.nextBoolean()){//add or subtract?
    		newRandomSharePrice = share.getSharePrice()+rng.nextInt(10);
    	}
    	else{
    		newRandomSharePrice = share.getSharePrice()-rng.nextInt(10);
    		if(newRandomSharePrice<1){
    			newRandomSharePrice = newRandomSharePrice*-1;// to prevent negative shareprices
    		}
    		if(newRandomSharePrice == 0){
    			newRandomSharePrice = 1;
    		}
    	}	
    	share.setSharePrice(newRandomSharePrice);  		
	}   	
}