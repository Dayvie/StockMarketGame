package stockMarketGame.stockMarket.gameLogic.stockMarket;

import java.util.Random;
/**
 * 
 * @author David
 * A StockPriceProvider which increase/decreases the shareprice by 1
 */
public class IncrementalStockPriceProvider extends StockPriceProvider {

	@Override
	public void updateSharePrice(Share share) {
		Random rng = new Random();
		if(rng.nextBoolean()){
			share.setSharePrice(share.getSharePrice()+1);
		}else{
			if(share.getSharePrice()-1 >0){
				share.setSharePrice(share.getSharePrice()-1);
			}		
		}
	}
}