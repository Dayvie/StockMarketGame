package stockMarketGame.stockMarket.gameLogic.stockMarket;
import stockMarketGame.stockMarket.gameLogic.exceptions.*;
/**
 * @author David Yesil
 * A share is an object which holds a name and a value 
 */
public class Share implements Comparable<Share>{
	
	private final String name;
	private long sharePrice;
	
	/**
	 * Defines the attributes of a share.
	 * @param shareName,
	 * @param sharePrice: The name of the share can not be null and the shareprice can not be below one.
	 * A NullParameterExceptions is thrown if the parameters were set wrong.
	 * The shareprice is represented in cents. 
	 */
	public Share(String shareName, long sharePrice)throws NullParameterException{		
		if(shareName == null | sharePrice <= 0){
			throw new NullParameterException();
		}
		this.name = shareName;
		this.sharePrice = sharePrice;		
	}
	
	public long getSharePrice(){		
		return this.sharePrice;		
	}	
	
	public String getName(){			
		return this.name;	
	}
	
	@Override
	public String toString(){		
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("Name of share: ");
		sBuilder.append(this.name);
		sBuilder.append(" | Shareprice: ");
		sBuilder.append(this.sharePrice);			
		return sBuilder.toString();		
	}
	
	public void setSharePrice(long sharePrice){
		this.sharePrice = sharePrice;
	}
	/**
	 * 
	 */
	@Override
	public boolean equals(Object object){
			
		if (this == object){ //same objects? two references for one object?
			return true;
		}
		if (object == null){//parameter null?
			return false;
		}		
		if (object.getClass() != this.getClass()){ //same class?
			return false;
		}		
		if(this.getSharePrice() != ((Share)object).getSharePrice()){//same SharePrice? (primitive data type)
			return false;
		}		
		if (!(this.name.equals(((Share)object).getName()))){ //same name? (reference-type)				
				return false;				
		}									
		return true;		
	}
	/**
	 * Compares shares by their names.
	 */
	@Override
	public int compareTo(Share share) {
	    return this.name.compareTo(share.getName());
	}
}