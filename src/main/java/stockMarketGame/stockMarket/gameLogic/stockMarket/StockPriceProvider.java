package stockMarketGame.stockMarket.gameLogic.stockMarket;

import java.util.ResourceBundle;
import java.util.Set;
import java.util.TreeSet;

import stockMarketGame.stockMarket.gameLogic.exceptions.ShareNotFoundException;
/**
 * Class which contains the Logic for the Ticker
 * @author David
 *
 */
public abstract class StockPriceProvider implements StockPriceInfo {
	
	private StockPriceViewer stockPriceViewer;
	protected Set<Share> shares;
	
	public StockPriceProvider() {	
		this.shares = new TreeSet<>();
		this.shares.add(new Share("Siemens",3));
		this.shares.add(new Share("BMW",5));
		this.shares.add(new Share("DeutscheBank",2));
		this.shares.add(new Share("Lufthansa", 6));
		this.shares.add(new Share("Daimler", 3));
		this.shares.add(new Share("DeutschePost",7));
		this.shares.add(new Share("Adidas", 10));
		this.stockPriceViewer = new StockPriceViewer(this);		
	}
	
	public Share searchShare(String shareName){ // do other classes need this method?		
		for(Share share:this.shares){
			if(share.getName().equals(shareName)){
				return share;
			}
		}
		throw new ShareNotFoundException();
	}
	
	@Override
	public String sharesToString() {
		ResourceBundle resources = ResourceBundle.getBundle("i18n/language");
		StringBuilder sBuilder = new StringBuilder();
		for(Share share:this.shares){				
			sBuilder.append("\n| ")
			.append(resources.getString("nameoftheshare"))
			.append(": ")
			.append(share.getName())
			.append("; ")
			.append(" ")
			.append(resources.getString("shareprice"))
			.append(": ")
			.append(share.getSharePrice())
			.append(" ")
			.append(resources.getString("currency"))
			.append(" |");
		}		
		return sBuilder.toString();
	}	
	@Override
	public StockPriceViewer getStockPriceViewer(){
		return this.stockPriceViewer;
	}
	@Override
	public long getSharePrice(String shareName)throws ShareNotFoundException {		
		return this.searchShare(shareName).getSharePrice();		
	}
	
	@Override
	public Share[] getAllSharesAsSnapshot() {//array erstellen
		return this.shares.toArray(new Share[this.shares.size()]);		
	}
	
	@Override
	public void startUpdate(){
		Ticker singleTicker = Ticker.getInstance();
		singleTicker.scheduleAtFixedRate(new UpdateSharesTask(this), 0, 500); //updateSharePrices()   
	}
	
	@Override
	public void updateSharePrices(){   	
		Share[] sharesArr = this.getAllSharesAsSnapshot();	
    	for(Share share:sharesArr){
    		this.updateSharePrice(share);	
    	}	      	
	}	
	@Override
	public abstract void updateSharePrice(Share share);
}