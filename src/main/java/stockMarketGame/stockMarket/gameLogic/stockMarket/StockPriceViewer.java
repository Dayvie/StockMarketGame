package stockMarketGame.stockMarket.gameLogic.stockMarket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.TimerTask;

import javafx.application.Platform;
import javafx.scene.control.Label;

public class StockPriceViewer extends Label {
	/**
	 * shows stocks on a GUI
	 */
	private StockPriceInfo stockPriceInfo;
    
    private class TickerTask extends TimerTask {
        @Override
		public void run() {
        	Platform.runLater(
                ()-> {
                    String output = TickerTask.this.createText();
                    StockPriceViewer.this.setText(output); 
                }
            );                
        }

        private String createText() {     
        	ResourceBundle resources = ResourceBundle.getBundle("i18n/language");
        	SimpleDateFormat sdf = new SimpleDateFormat(resources.getString("dateformat"));     	
        	StringBuilder sbuilder = new StringBuilder();
        	sbuilder
        	.append(sdf.format(new Date()).toString())
        	.append("\n")
        	.append(resources.getString("shares"))
        	.append(": ")
        	.append(StockPriceViewer.this.stockPriceInfo.sharesToString());
            return sbuilder.toString();
        }
    }

    public StockPriceViewer(StockPriceInfo stockPriceProvider) {
    	super();
    	this.stockPriceInfo = stockPriceProvider;
        Ticker singleTicker = Ticker.getInstance();
        singleTicker.scheduleAtFixedRate(new TickerTask(), 0, 250);
    }
}