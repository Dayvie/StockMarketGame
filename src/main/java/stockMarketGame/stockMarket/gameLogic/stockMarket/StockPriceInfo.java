package stockMarketGame.stockMarket.gameLogic.stockMarket;


public interface StockPriceInfo {
	String sharesToString();
	long getSharePrice(String shareName);
	Share[] getAllSharesAsSnapshot();
	void updateSharePrice (Share share);
	void startUpdate();
	void updateSharePrices();
	StockPriceViewer getStockPriceViewer();
}