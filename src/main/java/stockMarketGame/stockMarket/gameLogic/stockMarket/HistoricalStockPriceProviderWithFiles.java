package stockMarketGame.stockMarket.gameLogic.stockMarket;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
/**
 * StockpriceProvider which bases its share prices on share prices of google finances. Requires an internet connection to work
 * @author David
 *
 */
@Deprecated
public class HistoricalStockPriceProviderWithFiles extends StockPriceProvider {

	private Map<String, Reader> readers;	
	private static final String PATH = "C:/Users/"+System.getProperty("user.name")+"/Desktop/BoersenSpiel/StockHistory/";
	
	public HistoricalStockPriceProviderWithFiles(){
		super();
		File file = new File(PATH);
		file.mkdirs();
		this.readers = new HashMap<>();
		this.downloadStockFiles();		
		for(Share share:this.shares){			
			file = new File(PATH + "/" + share.getName() + ".csv");
			try {			
				BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
				bufferedReader.readLine(); // skip first line
				this.readers.put(share.getName(), bufferedReader);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}			
			catch (IOException e) {
				e.printStackTrace();
			}			
		}	
	}
	/**
	 * @param share share which will be updated
	 * 
	 * Updates a share based on a historical shareprice which is read from a .csv file
	 */
	@Override
	public void updateSharePrice(Share share){			
		BufferedReader bufferedReader = (BufferedReader)this.readers.get(share.getName());
		try {
			String readLine = bufferedReader.readLine();
			if(readLine == null){//csv file has no content left
				this.readers.remove(share.getName());
				bufferedReader.close();
				bufferedReader = new BufferedReader(new FileReader(PATH + "/" + share.getName() + ".csv")); // last reference to the reader gets overwritten
				bufferedReader.readLine(); //skip first line
				readLine = bufferedReader.readLine();
				this.readers.put(share.getName(), bufferedReader);//create new reader and start from the beginning		
			}
			Random rng = new Random();
			share.setSharePrice((long)Double.parseDouble(readLine.split(",")[rng.nextInt(3)+1]));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void downloadStockFiles(){
		HashMap<String, String> urls = new HashMap<>();		
		urls.put("Adidas", "http://www.google.com/finance/historical?q=ETR%3AADS&ei=-7NhVamTEceY8gPJtYOoDA&output=csv"); 
		urls.put("BMW", "http://www.google.com/finance/historical?q=FRA%3ABMW&ei=rLRhVenjC8Gh8APtmIPgDQ&output=csv"); 
		urls.put("DeutscheBank", "http://www.google.com/finance/historical?q=FRA%3ADBK&ei=HrVhVYHeNISf8gOSlYOoDA&output=csv"); 
		urls.put("Siemens", "http://www.google.com/finance/historical?q=FRA%3ASIE&ei=jrZhVZnsA8zr8QOc8IPwBA&output=csv");
		urls.put("Lufthansa", "http://www.google.com/finance/historical?q=ETR%3ALHA&ei=2bZhVcGoMcGh8APtmIPgDQ&output=csv");
		urls.put("Daimler", "http://www.google.com/finance/historical?q=OTCMKTS%3ADDAIF&ei=jrdhVcj_IcLA8gPVuYO4DQ&output=csv");
		urls.put("DeutschePost", "http://www.google.com/finance/historical?q=ETR%3ADPW&ei=ybdhVenIFIXZ8gOXpoCoBA&output=csv");
		for(Share share:this.shares){
			try (InputStream bufferedInputStream =
					new BufferedInputStream(new URL(urls.get(share.getName())).openStream());
					ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
					FileOutputStream fileOutPutStream = new FileOutputStream(PATH + share.getName() + ".csv");){							
						 // open connection to this URL
						byte[] bufferArray = new byte[1024];
						int n = 0;
						while (-1!=(n=bufferedInputStream.read(bufferArray))){ // read URL content, returns -1 if end of stream is reached
							byteArrayOutputStream.write(bufferArray, 0, n); // write URL content in outputstream, 0 = offset, n = length of bytes which are written
						}
						byte[] fileData = byteArrayOutputStream.toByteArray();		
						fileOutPutStream.write(fileData);
			} catch(MalformedURLException e) {
				e.printStackTrace();
			} catch(IOException e) {
				e.printStackTrace();
			}	
		}
	}
}