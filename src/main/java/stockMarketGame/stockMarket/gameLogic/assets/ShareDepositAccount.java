package stockMarketGame.stockMarket.gameLogic.assets;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import stockMarketGame.stockMarket.gameLogic.exceptions.*;
import stockMarketGame.stockMarket.gameLogic.stockMarket.Share;

public class ShareDepositAccount extends Asset {

	private Map<String, ShareItem> shareItems;
	
	public ShareDepositAccount(String owner)throws NullParameterException{	
		if(owner == null){
			throw new NullParameterException();
		}
		this.name = owner;
		this.shareItems = new HashMap<>();
	}
	
	public ShareItem[] getShareItems(){
		return this.shareItems.values().toArray(new ShareItem[0]);
	}
	/**
	 * Method which adds shareitems to the sharedepositaccount. Does only create new shareitems if shareitems with the same sharename don't exit.
	 * @param share bought share
	 * @param amount amount bought
	 */
	public synchronized void buyShare(Share share, int amount){	
		ShareItem shareItem = this.shareItems.get(share.getName());
		if(shareItem != null){
			shareItem.buyShare(amount);
			this.update();
			return;
		}
		this.shareItems.put(share.getName(),new ShareItem(share.getName(), new Share(share.getName(), share.getSharePrice()), amount));
		this.update();
		return;
	}
	
	/**
	 * Method which removes shareitems from the sharedepositaccount. Does only remove whole shareitem objects if the amount of a shareitem becomes 0
	 * @param share sold share
	 * @param amount amount sold
	 * @throws ShareNotFoundException if a shareitem isn't stored in the sharedepositaccount
	 */
	public synchronized void sellShare(Share share, int amount)throws ShareNotFoundException{	
		ShareItem shareItem = this.shareItems.get(share.getName());
		if(shareItem !=null){
			if(shareItem.getAmount()-amount<0){
				throw new TransactionException();
			}
			shareItem.sellShare(amount);
			if(shareItem.getAmount() == 0){
				this.shareItems.remove(share.getName());
			}
			this.update();
			return;
		}
		throw new ShareNotFoundException();					
	}
	
	public synchronized void update(){ //recalc the value of the ShareDepositAccount
		Long newValue = (long)0;
		for(ShareItem shareItem: this.shareItems.values()){
			newValue += shareItem.getValue();
		}					
		this.value= newValue;
	}
		
	@Override
	public String toString(){
		this.update();
		ResourceBundle resources = ResourceBundle.getBundle("i18n/language");
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append(" | "+ resources.getString("valueof")+ " ")
		.append(this.name)
		.append("'s "+ resources.getString("shareitems")+ ": ")
		.append(this.getValue())
		.append(" ")
		.append(resources.getString("currency"))
		.append(" ")
		.append(" | "+ resources.getString("aktienpakete")+ ": ");		
		for(ShareItem shareItem: this.shareItems.values()){
			sBuilder.append(shareItem.getName())
			.append(", "+ resources.getString("value")+ ": "+shareItem.getValue())
			.append(" ")
			.append(resources.getString("currency"))
			.append(", " + resources.getString("amount") + ": "+shareItem.getAmount())
			.append("; ");
		}
		return sBuilder.toString();		
	}
}