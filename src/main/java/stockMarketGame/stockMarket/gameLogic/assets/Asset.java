package stockMarketGame.stockMarket.gameLogic.assets;
public abstract class Asset {
	
	protected String name;
	protected long value;
			
	public long getValue(){		
		return this.value;		
	}
	
	public String getName(){		
		return this.name;
	}
	
	@Override
	public abstract String toString();
}