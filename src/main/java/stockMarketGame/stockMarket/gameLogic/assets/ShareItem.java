package stockMarketGame.stockMarket.gameLogic.assets;
import stockMarketGame.stockMarket.gameLogic.exceptions.*;
import stockMarketGame.stockMarket.gameLogic.stockMarket.Share;
/**
 * An object which holds only one type of shares. 
 * @author David
 *
 */
public class ShareItem extends Asset {

	private int amount;
	private Share share;
	
	public ShareItem(String shareName, Share share, int amount)throws NullParameterException, EmptyShareItemException{
		if(shareName == null | share == null){
			throw new NullParameterException();
		}
		if(amount == 0){
			throw new EmptyShareItemException();
		}
		this.name = shareName ;
		this.amount = amount;
		this.share = share;
		this.update();
	}
	
	public Share getShare(){
		return this.share;
	}
	
	public synchronized void update(){ //recalc the value of the ShareItem
		this.value = (this.amount*this.share.getSharePrice());
	}
	
	/**
	 * Increases the amount of the share
	 * @param amount
	 * @throws TransactionException
	 */
	public synchronized void buyShare(int amount)throws TransactionException{
		if(amount <=0){
			throw new TransactionException();
		}
		this.amount+=amount;
		this.update();
	}
	
	/**
	 * Decreases the amount of the share
	 * @param amount
	 * @throws TransactionException
	 */
	public synchronized void sellShare(int amount)throws TransactionException{
		if(amount <=0){
			throw new TransactionException();
		}
		this.amount-=amount;
		this.update();
	}
	
	public int getAmount(){
		return this.amount;
	}
		
	@Override
	public String toString(){
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("Name of Shareitem: ");
		sBuilder.append(this.getName());
		sBuilder.append(" | Value: ");
		sBuilder.append(this.getValue());
		sBuilder.append(" | Amount: ");
		sBuilder.append(this.amount);
		return sBuilder.toString();
	}
	
	@Override
	public boolean equals(Object object){
			
		if (this == object){ //same objects? two references for one object?
			return true;
		}
		if (object == null){//parameter null?
			return false;
		}		
		if (object.getClass() != this.getClass()){ //same class?
			return false;
		}		
		if(this.amount != ((ShareItem)object).getValue()){//same SharePrice? (primitive data type)
			return false;
		}		
		if (!(this.name.equals(((ShareItem)object).getName()))){ //same name? (reference-type)				
				return false;				
		}			
		if (!(this.share.equals(((ShareItem)object).getShare()))){ //same name? (reference-type)				
			return false;				
		}
		return true;		
	}
}