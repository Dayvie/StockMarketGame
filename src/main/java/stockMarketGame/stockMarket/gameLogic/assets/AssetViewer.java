package stockMarketGame.stockMarket.gameLogic.assets;
import java.util.TimerTask;

import javafx.application.Platform;
import javafx.scene.control.Label;
import stockMarketGame.stockMarket.gameLogic.accountManager.*;
import stockMarketGame.stockMarket.gameLogic.stockMarket.Ticker;

public class AssetViewer extends Label{
	/**
	 * shows playerstats on a GUI
	 */
	private AccountManager accManag;
	
    private class AssetTask extends TimerTask {
        @Override
		public void run() {
        	Platform.runLater(new Runnable(){
                @Override
                public void run() {
                    String output = AssetTask.this.createText();
                    AssetViewer.this.setText(output); 
                }
            });      	   
        }
        private String createText() {             	
            StringBuilder sBuilder = new StringBuilder();
            AssetViewer.this.accManag.updatePlayers();
            sBuilder.append(AssetViewer.this.accManag.playersToString());
            Player[] players = AssetViewer.this.accManag.getPlayers();
            for (Player player : players) {
            	sBuilder.append(AssetViewer.this.accManag.shareDepositAccountToString((player).getName())+"\n");               
            	}

            return sBuilder.toString();
        }
    }

    public AssetViewer(AccountManager accManag) {
    	super();
    	this.accManag = accManag;
        Ticker singleTicker = Ticker.getInstance();
        singleTicker.scheduleAtFixedRate(new AssetTask(), 0, 250);
    }
}