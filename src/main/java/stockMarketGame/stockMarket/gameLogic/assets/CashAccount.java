package stockMarketGame.stockMarket.gameLogic.assets;
import stockMarketGame.stockMarket.gameLogic.exceptions.*;

public class CashAccount extends Asset {
	
	public CashAccount(String owner, long money)throws NullParameterException{		
		if(owner==null){
			throw new NullParameterException();
		}
		this.name = owner;
		this.value = money;	
	}
	
	public synchronized void deposit(long money)throws TransactionException{
		if(money<=0){
			throw new TransactionException();
		}
		this.value+= money;		
	}
	
	public synchronized void withdraw(long money)throws TransactionException, NotEnoughMoneyException{
		if(money<=0){
			throw new TransactionException();
		}
		if(this.value-money<0){
			throw new NotEnoughMoneyException();
		}	
		this.value-=money;
	}
	
	@Override
	public String toString(){
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("Owner of the account: ");
		sBuilder.append(this.getName());
		sBuilder.append(" | ");
		sBuilder.append("Money in the CashAccount: ");
		sBuilder.append(+this.getValue());
		return sBuilder.toString();	
	}
}