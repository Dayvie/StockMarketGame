package stockMarketGame.stockMarket.gameLogic.exceptions;

public class PlayerExistsException extends RuntimeException{
	
	public PlayerExistsException(){
		
		super();
		
	}
	public PlayerExistsException(String s){
		
		super(s);
		
	}
	
	@Override
	public String toString(){
		
		return "Player has already been created -> aborting creation";
		
	}
	
}
