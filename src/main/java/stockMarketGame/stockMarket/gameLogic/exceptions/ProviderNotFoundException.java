package stockMarketGame.stockMarket.gameLogic.exceptions;

public class ProviderNotFoundException extends RuntimeException {
	
	public ProviderNotFoundException(){
		super();
	}
	
	public ProviderNotFoundException(String s){
		super(s);
	}
	
	@Override
	public String toString(){
		return "ProviderNotFound";
	}

}
