package stockMarketGame.stockMarket.gameLogic.exceptions;

public class NullParameterException extends RuntimeException {

	public NullParameterException(){
		super();
	}
	
	public NullParameterException(String s){
		super(s);
	}
	
	@Override
	public String toString(){
		
		return "ShareNameIsNull";
		
	}
}
