package stockMarketGame.stockMarket.gameLogic.exceptions;

public class NotEnoughMoneyException extends RuntimeException {
	
	public NotEnoughMoneyException(){
		super();
	}
	public NotEnoughMoneyException(String s) {
		super(s); 
	}
	@Override
	public String toString(){
		
		return "You can not afford this item";
		
	}
	
}
