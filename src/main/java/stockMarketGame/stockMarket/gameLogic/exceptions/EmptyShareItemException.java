package stockMarketGame.stockMarket.gameLogic.exceptions;

public class EmptyShareItemException extends RuntimeException{
	
	public EmptyShareItemException(){
		super();
	}
	
	@Override
	public String toString(){
		return "cannot create empty shareItems";
	}
}
