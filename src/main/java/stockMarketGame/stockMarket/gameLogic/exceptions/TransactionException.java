package stockMarketGame.stockMarket.gameLogic.exceptions;

public class TransactionException extends RuntimeException {
	
	public TransactionException(){
		super();
	}

	public TransactionException(String s){
		super(s);
	}
	
	@Override
	public String toString(){
		return "nonsensical transaction";
	}
}
