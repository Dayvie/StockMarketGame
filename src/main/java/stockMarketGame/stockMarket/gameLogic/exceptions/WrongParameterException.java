package stockMarketGame.stockMarket.gameLogic.exceptions;

public class WrongParameterException extends RuntimeException {

	public WrongParameterException(){		
		super();		
	}
	
	public WrongParameterException(String s){		
		super(s);		
	}
	
	@Override
	public String toString(){		
		return "You have entered wrong parameters";		
	}
}
