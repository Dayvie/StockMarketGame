package stockMarketGame.stockMarket.main;

import java.util.ResourceBundle;

import stockMarketGame.stockMarket.commandShell.UniversialGuiCommandProcessor;
import stockMarketGame.stockMarket.gameLogic.accountManager.AccountManager;

public class Controller {
	
	private AccountManager accountManager;
	private UniversialGuiCommandProcessor guiCommandProcessor;
	private ResourceBundle languageResources;
	
	public Controller(){
		this.languageResources = ResourceBundle.getBundle("i18n/language");
		
	}

	public AccountManager getAccountManager() {
		return accountManager;
	}

	public void setAccountManager(AccountManager accountManager) {
		this.accountManager = accountManager;
	}

	public UniversialGuiCommandProcessor getGuiCommandProcessor() {
		return guiCommandProcessor;
	}

	public void setGuiCommandProcessor(
			UniversialGuiCommandProcessor guiCommandProcessor) {
		this.guiCommandProcessor = guiCommandProcessor;
	}

	public ResourceBundle getLanguageResources() {
		return languageResources;
	}
	
	

}
