package stockMarketGame.stockMarket.main;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class PopUpError extends Stage {
	
	Label errorMessage;
	Button okButton;
	
	public PopUpError(String message){
		this.errorMessage = new Label(message);
		VBox vBox = new VBox();
		vBox.setPadding(new Insets(50,50,50,50));
		BorderPane borderPane = new BorderPane();
		setScene(new Scene(borderPane, 300, 300));		
		this.okButton = new Button("OK");
		this.okButton.setPrefSize(200, 100);
		this.okButton.setOnAction(e ->{
			this.close();
		});
		ScrollPane sp = new ScrollPane();
		sp.setContent(this.errorMessage);
		vBox.getChildren().addAll(sp, this.okButton);
		borderPane.setCenter(vBox);	
		this.show();
	}
}