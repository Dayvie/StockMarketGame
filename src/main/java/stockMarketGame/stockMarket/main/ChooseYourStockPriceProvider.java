package stockMarketGame.stockMarket.main;
import java.lang.reflect.Proxy;
import java.util.ResourceBundle;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import stockMarketGame.stockMarket.commandShell.UniversialGuiCommandProcessor;
import stockMarketGame.stockMarket.commandShell.UniversialShellCommandProcessor;
import stockMarketGame.stockMarket.gameLogic.accountManager.AccountManager;
import stockMarketGame.stockMarket.gameLogic.accountManager.AccountManagerImpl;
import stockMarketGame.stockMarket.gameLogic.accountManager.DynamicAccountManagerProxy;
import stockMarketGame.stockMarket.gameLogic.stockMarket.ConstantStockPriceProvider;
import stockMarketGame.stockMarket.gameLogic.stockMarket.RandomStockPriceProvider;
import stockMarketGame.stockMarket.gameLogic.stockMarket.HistoricalStockPriceProviderWithFiles;
import stockMarketGame.stockMarket.gameLogic.stockMarket.StockPriceProvider;
import stockMarketGame.stockMarket.gameLogic.stockMarket.IncrementalStockPriceProvider;

public class ChooseYourStockPriceProvider extends Stage {
	
	private AccountManager accountManager;
	private UniversialGuiCommandProcessor guiCommandProcessor;
	private Label returnLabel;
	private ResourceBundle languageResources;
	
	public ChooseYourStockPriceProvider(AccountManager accountManager, UniversialGuiCommandProcessor guiCommandProcessor){
		super();
		this.setTitle("Choose wisely");
		this.languageResources = ResourceBundle.getBundle("i18n/language");
		this.returnLabel = new Label(this.languageResources.getString("result"));
		HBox hBox = new HBox();
		hBox.getChildren().addAll(generateConstantStockPriceButton(), generateRandomStockPriceButton(), generateHistoricalStockPriceButton(),
				generateIncrementalStockPriceButton());
		Scene scene = new Scene (hBox, 1200, 300);
		this.setScene(scene);
		this.accountManager = accountManager;	
		this.show();
	}
	
	private Button generateConstantStockPriceButton(){
		Button button = new Button("ConstantStockpriceProvider");
		button.setOnAction(e->{
			this.accountManager = createProxy(new ConstantStockPriceProvider());	 
			this.close();
			startGame();
		});
		button.setPrefSize(300, 300);
		return button;
	}
	
	private Button generateRandomStockPriceButton(){
		Button button = new Button("RandomStockpriceProvider");
		button.setOnAction(e->{
			this.accountManager = createProxy(new RandomStockPriceProvider());
			this.close();
			startGame();
		});
		button.setPrefSize(300, 300);
		return button;
	}
	
	private Button generateHistoricalStockPriceButton(){
		Button button = new Button("HistoricalStockPriceProvider");
		button.setOnAction(e->{
			this.accountManager = createProxy(new HistoricalStockPriceProviderWithFiles());
			this.close();
			startGame();
		});
		button.setPrefSize(300, 300);
		return button;
	}
	
	private Button generateIncrementalStockPriceButton(){
		Button button = new Button("IncrementalStockPriceProvider");
		button.setOnAction(e->{
			this.accountManager = createProxy(new IncrementalStockPriceProvider());
			this.close();
			startGame();
		});
		button.setPrefSize(300, 300);
		return button;
	}
	
	private AccountManager createProxy(StockPriceProvider stockPriceProvider){
		return (AccountManager) Proxy.newProxyInstance(
				AccountManager.class.getClassLoader(),
	            new Class[] {AccountManager.class},
	            new DynamicAccountManagerProxy(new AccountManagerImpl(stockPriceProvider)));
	}
	
	private void startGame(){
		this.guiCommandProcessor = new UniversialGuiCommandProcessor(this.accountManager, AccountManager.class, this.returnLabel);
		new GameStage(accountManager, guiCommandProcessor, returnLabel);
		close();
	}
}