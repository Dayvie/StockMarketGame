package stockMarketGame.stockMarket.main;

import java.io.IOException;
import stockMarketGame.stockMarket.commandShell.UniversialGuiCommandProcessor;
import stockMarketGame.stockMarket.gameLogic.accountManager.AccountManager;
import stockMarketGame.stockMarket.gameLogic.exceptions.NullParameterException;
import javafx.application.Application;

import javafx.stage.Stage;

/**
 * Class which initializes the Game Logic, the CommandProcessors, and the GUI.
 * @author David
 *
 */
public class Main extends Application {
	
	private AccountManager accountManager;
	private UniversialGuiCommandProcessor guiCommandProcessor;
	
	public static void main(String[] args) throws NullParameterException, IOException{		
		Application.launch();	
	}	
	
	@Override
	public void start(Stage primaryStage) throws Exception{
		primaryStage = new ChooseYourStockPriceProvider(this.accountManager, this.guiCommandProcessor);
	}
}