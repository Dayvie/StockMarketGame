package stockMarketGame.stockMarket.main;

import java.util.ResourceBundle;
import java.util.TimerTask;

import stockMarketGame.stockMarket.gameLogic.accountManager.AccountManager;
import stockMarketGame.stockMarket.gameLogic.accountManager.MimeType;
import stockMarketGame.stockMarket.gameLogic.accountManager.Player;
import stockMarketGame.stockMarket.gameLogic.stockMarket.Share;
import stockMarketGame.stockMarket.gameLogic.stockMarket.Ticker;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

public class PlayerOptions extends HBox {

	private Label playerOptionsLabel;
	private ChoiceBox<String>choosePlayer;
	private TextField chooseAmount;
	private Button proAgButton;
	private Button rndAgButton;
	private Button printAccountStatementsButton;
	private ChoiceBox<String>sortingCriteriaChoiceBox;
	private ChoiceBox<MimeType>mimeTypeChoiceBox;
	private Label returnLabel;
	private AccountManager accountManagerImpl;
	private ResourceBundle languageResources;
	
	public PlayerOptions(AccountManager accountManagerImpl, Label returnLabel, TextField chooseAmount){
		super();
		this.languageResources = ResourceBundle.getBundle("i18n/language");
		initPlayerChoiceBoxTicker();
		this.returnLabel = returnLabel;
		this.accountManagerImpl = accountManagerImpl;
		this.mimeTypeChoiceBox = generateMimeTypeChoiceBox();
		this.sortingCriteriaChoiceBox = generateSortingCriteriaChoiceBox();
		this.playerOptionsLabel = new Label(this.languageResources.getString("playeroptions")+"\t\t");
		this.choosePlayer = new ChoiceBox<>();
		this.chooseAmount = chooseAmount;
		this.proAgButton = generateProAgButton();
		this.rndAgButton = generateRndAgButton();
		this.printAccountStatementsButton = generatePrintAccountStatementsButton();
		this.getChildren().addAll(playerOptionsLabel, choosePlayer, chooseAmount, proAgButton, rndAgButton,
				printAccountStatementsButton, sortingCriteriaChoiceBox, mimeTypeChoiceBox);
	}
	
	private ChoiceBox<MimeType> generateMimeTypeChoiceBox(){	
		ChoiceBox<MimeType> mimeTypeChoiceBox = new ChoiceBox<>();
		mimeTypeChoiceBox.getItems().addAll(MimeType.FILE, MimeType.SCREEN);	
		mimeTypeChoiceBox.setValue(MimeType.FILE);
		return mimeTypeChoiceBox;
	}
	
	private Button generatePrintAccountStatementsButton(){
		Button printAccountStatements = new Button("AccountStatements");
		printAccountStatements.setOnAction(e->{
		this.returnLabel.setText(this.accountManagerImpl.printAccountStatements(this.choosePlayer.getValue(),
				this.sortingCriteriaChoiceBox.getValue(),
				this.mimeTypeChoiceBox.getValue()));
		});		
		return printAccountStatements;
	}

	private ChoiceBox<String> generateSortingCriteriaChoiceBox(){	
		ChoiceBox<String> sortingCriteriaChoiceBox = new ChoiceBox<String>();
		sortingCriteriaChoiceBox.getItems().addAll("Time", "Share");	
		Share[] shares = this.accountManagerImpl.getStockPriceProvider().getAllSharesAsSnapshot();
		for(Share share:shares){
			sortingCriteriaChoiceBox.getItems().add(share.getName());
		}	
		sortingCriteriaChoiceBox.setValue("Time");
		return sortingCriteriaChoiceBox;
	}
	
	/**
	 * @author David Yesil
	 * Updates the PlayerName-Strings in the Drop-Down menu
	 */
	private void updateChoosePlayer(){
		Player[] players = accountManagerImpl.getPlayers();
		for(Player player:players){
			if(this.choosePlayer.getItems().contains(player.getName())){
				continue;
			}					
			choosePlayer.getItems().add(player.getName());
		}
		if(players.length == 1){
			choosePlayer.setValue(players[0].getName());
		}		
	}
	
	private Button generateRndAgButton(){
		final Button rndag = new Button();
		rndag.setText("RandomAgent");
		rndag.setOnAction(e->{
			accountManagerImpl.createRngAgent(choosePlayer.getValue());
		});	
		return rndag;
	}
	
	private Button generateProAgButton(){
		final Button proag = new Button();
		proag.setText("ProfitAgent");
		proag.setOnAction(e->{
			accountManagerImpl.createProfitAgent(choosePlayer.getValue());
		});
		return proag;
	}
	/**
	 * @author David Yesil
	 * Starts a TimerTask which executes *updateChoosePlayer()* regularly
	 */
	private void initPlayerChoiceBoxTicker(){
		Ticker singleTicker = Ticker.getInstance();
        singleTicker.scheduleAtFixedRate(new TimerTask(){
			@Override
			public void run() {
				Platform.runLater(new Runnable(){
                @Override
                public void run() {
                	updateChoosePlayer();
                }
				});      	  				
			}        	
        }, 0, 250);
	}
}