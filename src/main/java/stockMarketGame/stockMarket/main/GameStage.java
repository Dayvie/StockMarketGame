package stockMarketGame.stockMarket.main;

import java.util.ResourceBundle;
import java.util.TimerTask;

import stockMarketGame.stockMarket.commandShell.UniversialGuiCommandProcessor;
import stockMarketGame.stockMarket.gameLogic.accountManager.AccountManager;
import stockMarketGame.stockMarket.gameLogic.accountManager.MimeType;
import stockMarketGame.stockMarket.gameLogic.accountManager.Player;
import stockMarketGame.stockMarket.gameLogic.exceptions.WrongParameterException;
import stockMarketGame.stockMarket.gameLogic.stockMarket.Share;
import stockMarketGame.stockMarket.gameLogic.stockMarket.Ticker;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class GameStage extends Stage {
	
	private AccountManager accountManager;
	private UniversialGuiCommandProcessor guiCommandProcessor;
	private ResourceBundle languageResources;
	private Label returnLabel;
	private ChoiceBox<String> choosePlayer;
	private TextField chooseAmount;
	private ChoiceBox<String>SortingCriteriaChoiceBox;
	private ChoiceBox<MimeType>MimeTypeChoiceBox;

	public GameStage(AccountManager accountManager, UniversialGuiCommandProcessor guiCommandProcessor,
		Label returnLabel){
		this.chooseAmount = new TextField("1");
		this.choosePlayer= new ChoiceBox<>();		
		this.languageResources = ResourceBundle.getBundle("i18n/language");
		this.returnLabel = returnLabel;	
		this.accountManager = accountManager;
		this.guiCommandProcessor = guiCommandProcessor;
		this.SortingCriteriaChoiceBox = generateSortingCriteriaChoiceBox();
		this.MimeTypeChoiceBox = generateMimeTypeChoiceBox();	
		initPlayerChoiceBoxTicker();
		this.choosePlayer = new ChoiceBox<>();
		BorderPane borderPane = new BorderPane();	
		setScene(new Scene(borderPane , 1280, 720));
		final Label tabLabel = new Label(this.languageResources.getString("playeroptions")+"\t\t");
		HBox playerOptions = new HBox(tabLabel, this.choosePlayer, this.chooseAmount,
				generateProAgButton(), generateRndAgButton(), generateDelAgButton(),
				generatePrintAccountStatementsButton(), this.SortingCriteriaChoiceBox,
				this.MimeTypeChoiceBox);
		final Label playerCreationText = new Label(this.languageResources.getString("playerCreation")+"\t\t");		
		HBox playerCreation = new HBox(playerCreationText, generatePlayerCreationShell());
		VBox vboxCenter = new VBox(10, 
				accountManager.getStockPriceProvider().getStockPriceViewer(), generateAssetViewerScrollPane(),
				playerOptions, generateBuyShareButtons(), generateSellShareButtons(),
				generateProfitButtons(), playerCreation);
		VBox vBoxBottom = new VBox(10, generateStartButton(), generateCommandTextField());		
		borderPane.setBottom(vBoxBottom);	
		borderPane.setTop(generateTopMenuBar());
		borderPane.setCenter(vboxCenter);
		borderPane.setRight(generateReturnLabelScrollPane());
		setOnCloseRequest(e->{
			System.exit(0);
		});				
		setTitle("StockMarketGame");
		this.show();
	}
	
	private ScrollPane generateReturnLabelScrollPane(){
		ScrollPane sp = new ScrollPane();
		sp.setPrefSize(300, 120);
		sp.setContent(this.returnLabel);
		return sp;
	}
	
	private ScrollPane generateAssetViewerScrollPane(){
		ScrollPane assetViewerPane = new ScrollPane();
		assetViewerPane.setPrefSize(0, 280);
		assetViewerPane.setContent(accountManager.getAssetViewer());
		return assetViewerPane;
	}
	
	private MenuBar generateTopMenuBar(){
		final Menu options = new Menu(this.languageResources.getString("Options"));	
		options.getItems().add(generateFontsMenu());	
		MenuBar menuBar = new MenuBar();
		menuBar.getMenus().addAll(generateFileMenu(), options, generateHelpMenu());			
		return menuBar;
	}
	
	private Menu generateHelpMenu(){
		Menu help = new Menu(this.languageResources.getString("Help"));
		Menu commands = new Menu(this.languageResources.getString("Commands"));
		help.getItems().add(commands);
		commands.setOnAction(e-> {
			this.guiCommandProcessor.help();
		});	
		return help;
	}
	
	private Menu generateFontsMenu(){
		final Menu fonts = new Menu(this.languageResources.getString("Font"));
		fonts.getItems().add(generateTahomeFontMenu());
		fonts.getItems().add(generateArialFontMenu());
		fonts.getItems().add(generateFranklinGothicHeavyFontMenu());
		fonts.getItems().add(generateCalibriFontMenu());
		fonts.getItems().add(generateRegularFontMenu());
		return fonts;
	}
	
	private Menu generateFileMenu(){
		final Menu file = new Menu(this.languageResources.getString("File"));
		final Menu exit = new Menu(this.languageResources.getString("Exit"));
		file.getItems().add(exit);
		file.getItems().add(generateNewGameMenu());
		exit.setOnAction(e->{
			System.exit(0);
		});
		return file;
	}
	
	private Menu generateFranklinGothicHeavyFontMenu(){
		final Menu franklinGothicHeavy = new Menu("FranklinGothicHeavy");
		franklinGothicHeavy.setOnAction(e->{
			getScene().getStylesheets().clear();
			getScene().getStylesheets().add("fontStyle/fontFranklinGothicHeavy.css");
		});
		return franklinGothicHeavy;
	}
	
	private Menu generateCalibriFontMenu(){
		final Menu calibri = new Menu("Calibri");
		calibri.setOnAction(e->{
			getScene().getStylesheets().clear();
			getScene().getStylesheets().add("fontStyle/fontCalibri.css");
		});
		return calibri;
	}
	
	private Menu generateTahomeFontMenu(){
		final Menu tahoma = new Menu("Tahoma");
		tahoma.setOnAction(e->{
			getScene().getStylesheets().clear();
			getScene().getStylesheets().add("fontStyle/fontTahoma.css");
		});
		return tahoma;
	}
	
	private Menu generateArialFontMenu(){
		final Menu arial = new Menu("Arial");		
		arial.setOnAction(e->{
			this.getScene().getStylesheets().clear();
			this.getScene().getStylesheets().add("fontStyle/fontArial.css");			
		});
		return arial;		
	}
	
	private Menu generateRegularFontMenu(){
		final Menu regular = new Menu("Regular");		
		regular.setOnAction(e->{
			this.getScene().getStylesheets().clear();
			this.getScene().getStylesheets().add("fontStyle/fontRegular.css");			
		});
		return regular;		
	}
	
	private ChoiceBox<String> generateSortingCriteriaChoiceBox(){	
		ChoiceBox<String> sortingCriteriaChoiceBox = new ChoiceBox<String>();
		sortingCriteriaChoiceBox.getItems().addAll("Time", "Share");	
		Share[] shares = this.accountManager.getStockPriceProvider().getAllSharesAsSnapshot();
		for(Share share:shares){
			sortingCriteriaChoiceBox.getItems().add(share.getName());
		}	
		sortingCriteriaChoiceBox.setValue("Time");
		return sortingCriteriaChoiceBox;
	}
	
	private ChoiceBox<MimeType> generateMimeTypeChoiceBox(){	
		ChoiceBox<MimeType> mimeTypeChoiceBox = new ChoiceBox<>();
		mimeTypeChoiceBox.getItems().addAll(MimeType.FILE, MimeType.SCREEN);	
		mimeTypeChoiceBox.setValue(MimeType.FILE);
		return mimeTypeChoiceBox;
	}
	
	/**
	 * Generates a button which can be used to print Accountstatements. The parameter of the prinAccountStatement()
	 * method are given by the ChoiceBox<String> mimeTypeChoiceBox and ChoiceBox<String>sortingCriteriaChoiceBox
	 * @return
	 */
	private Button generatePrintAccountStatementsButton(){
		Button printAccountStatements = new Button("AccountStatements");
		printAccountStatements.setOnAction(e->{
		this.returnLabel.setText(this.accountManager.printAccountStatements(this.choosePlayer.getValue(),
				this.SortingCriteriaChoiceBox.getValue(),
				this.MimeTypeChoiceBox.getValue()));
		});		
		return printAccountStatements;
	}
	
	/**
	 * @author David Yesil
	 * Starts a TimerTask which executes *updateChoosePlayer()* regularly
	 */
	private void initPlayerChoiceBoxTicker(){
		Ticker singleTicker = Ticker.getInstance();
        singleTicker.scheduleAtFixedRate(new TimerTask(){
			@Override
			public void run() {
				Platform.runLater(new Runnable(){
                @Override
                public void run() {
                	updateChoosePlayer();
                }
				});      	  				
			}        	
        }, 0, 250);
	}
	
	/**
	 * Generates a textfield which represents a UniversialCommandProcessor
	 * @return an instance of said button
	 */
	private TextField generatePlayerCreationShell(){
		TextField playerCreationShell = new TextField();
		playerCreationShell.setOnAction(e->{
			accountManager.createPlayer(playerCreationShell.getText());
			playerCreationShell.clear();
		});		
		return playerCreationShell;
	}
	
	private TextField generateCommandTextField(){
		final TextField commandTextField = new TextField();		
		commandTextField.setOnAction(e->{					
			guiCommandProcessor.process(commandTextField.getText());
			commandTextField.clear();
		});
		return commandTextField;
	}
	
	private Button generateStartButton(){
		final Button startTicker = new Button();
		startTicker.setText(this.languageResources.getString("ticker"));	
		startTicker.setOnAction(e->{
			accountManager.startUpdate();
			startTicker.setVisible(false);
		});
		return startTicker;
	}
	
	private Button generateRndAgButton(){
		final Button rndag = new Button();
		rndag.setText("RandomAgent");
		rndag.setOnAction(e->{
			accountManager.createRngAgent(choosePlayer.getValue());
		});	
		return rndag;
	}
	/**
	 * Method which generates a Button whose Event Listener creates a profit agent which buys shares for the 
	 * player who is locked in the ChoiceBox<String> choosePlayer
	 * @return
	 */
	private Button generateProAgButton(){
		final Button proag = new Button();
		proag.setText("ProfitAgent");
		proag.setOnAction(e->
			accountManager.createProfitAgent(choosePlayer.getValue()));
		return proag;
	}
	
	private Button generateDelAgButton(){
		final Button delAg = new Button();
		delAg.setText("Delete agent");
		delAg.setOnAction(e->{
			accountManager.deleteAgent(choosePlayer.getValue());
		});
		return delAg;
	}
	
	private Menu generateNewGameMenu(){
		final Menu restartMenu = new Menu(this.languageResources.getString("newgame"));
		restartMenu.setOnAction(e->{
			new ChooseYourStockPriceProvider(this.accountManager, this.guiCommandProcessor);
			this.close();
		});
		return restartMenu;
	}
	
	/**
	 * @author David Yesil
	 * Updates the PlayerName-Strings in the Drop-Down menu
	 */
	private void updateChoosePlayer(){
		Player[] players = accountManager.getPlayers();
		for(Player player:players){
			if(this.choosePlayer.getItems().contains(player.getName())){
				continue;
			}					
			choosePlayer.getItems().add(player.getName());
		}
		if(players.length == 1){
			choosePlayer.setValue(players[0].getName());
		}		
	}
	
	/**
	 * @author David Yesil
	 * @param text which will be tested
	 * @return false if the textbox doesn't contain an integer and true if it does
	 */
	private boolean testForInt(String text) {
	      try {
	         Integer.parseInt(text);
	         return true;
	      } catch (NumberFormatException e) {
	         return false;
	      }
	}
	
	/**
	 * @author David Yesil
	 * Generates a Button for each share. Every of those buttons display a name of a share. 
	 * The displayed share is bought if the button is clicked
	 * @return the HBox which contains the buttons
	 */
	private HBox generateBuyShareButtons(){
		HBox shareHBox = new HBox();
		shareHBox.getChildren().add(new Label(this.languageResources.getString("purchase")+"\t"+"\t"));
		Share[] shares = this.accountManager.getStockPriceProvider().getAllSharesAsSnapshot();
		for(Share share:shares){
			Button buyShareButton = new Button();	
			buyShareButton.setText(share.getName());
			buyShareButton.setOnAction(e->{
				if(testForInt(chooseAmount.getText())){
					if(choosePlayer.getValue() == null) {
						return;
					}
					this.accountManager.buyShare(choosePlayer.getValue(), share.getName(), Integer.parseInt(chooseAmount.getText()));
				}else{
					new PopUpError("Please enter a number");
				}	
			});
			shareHBox.getChildren().add(buyShareButton);
		}
		return shareHBox;
	}
	
	/**
	 * @author David Yesil
	 * Generates a Button for each share. Every of those buttons displays a name of a share. 
	 * The displayed share is sold if the button is clicked
	 * @return the HBox which contains the buttons
	 */
	private HBox generateSellShareButtons(){
		HBox shareHBox = new HBox();
		shareHBox.getChildren().add(new Label(this.languageResources.getString("sell")+"\t"+"\t"));
		Share[] shares = this.accountManager.getStockPriceProvider().getAllSharesAsSnapshot();
		for(Share share:shares){
			Button sellShareButton = new Button();	
			sellShareButton.setText(share.getName());
			sellShareButton.setOnAction(e->{
				if(testForInt(chooseAmount.getText())){
					if(choosePlayer.getValue() == null){
						return;
					}
					this.accountManager.sellShare(choosePlayer.getValue(), share.getName(), Integer.parseInt(chooseAmount.getText()));
				}else{
					new PopUpError("Please enter a number");
				}
			});
			shareHBox.getChildren().add(sellShareButton);
		}
		return shareHBox;
	}
	
	/**
	 * @author David Yesil
	 * Generates a Button for each share. Every of those buttons display a name of a share. 
	 * The displayed share is compared if the button is clicked
	 * @return the HBox which contains the buttons
	 */
	private HBox generateProfitButtons(){
		HBox shareHBox = new HBox();
		shareHBox.getChildren().add(new Label(this.languageResources.getString("profitable?")+"\t"+"\t"));
		Share[] shares = this.accountManager.getStockPriceProvider().getAllSharesAsSnapshot();
		for(Share share:shares){
			Button sellShareButton = new Button();	
			sellShareButton.setText(share.getName());
			sellShareButton.setOnAction(e->{
				if(testForInt(chooseAmount.getText())){
					if(choosePlayer.getValue() == null){
						return;
					}
					if(this.accountManager.isShareMoreValuable(choosePlayer.getValue(), share.getName())){
						this.returnLabel.setText(this.languageResources.getString("result")+" "+languageResources.getString("profitable"));
					}else{
						this.returnLabel.setText(this.languageResources.getString("result")+" "+languageResources.getString("notprofitable"));
					}
				}else{
					new PopUpError("Please enter a number");
				}
			});
			shareHBox.getChildren().add(sellShareButton);
		}
		return shareHBox;
	}
}