package stockMarketGame.stockMarket.main;

public enum StockPriceProvider {
	HistoricalStockPriceProvider, RandomStockPriceProvider, ConstantStockPriceProvider;
}
