package stockMarketGame.stockMarket.commandShell;

import java.lang.reflect.Method;

public interface CommandTypeInfo {	
	String getCommand();
	String getDescription();
 	Class <?>[] getParamTypes();
 	Object getTarget();
 	Method getMethod() throws NoSuchMethodException, SecurityException;
}