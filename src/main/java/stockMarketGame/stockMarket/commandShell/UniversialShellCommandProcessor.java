package stockMarketGame.stockMarket.commandShell;
/*
 * 
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import stockMarketGame.stockMarket.commandShell.AsCommand;
import stockMarketGame.stockMarket.gameLogic.exceptions.NotEnoughMoneyException;
import stockMarketGame.stockMarket.gameLogic.exceptions.NullParameterException;
import stockMarketGame.stockMarket.gameLogic.exceptions.PlayerAlreadyExistsException;
import stockMarketGame.stockMarket.gameLogic.exceptions.PlayerNotFoundException;
import stockMarketGame.stockMarket.gameLogic.exceptions.ShareNotFoundException;
import stockMarketGame.stockMarket.gameLogic.exceptions.WrongParameterException;

	public class UniversialShellCommandProcessor {
		private BufferedReader shellReader;
		private PrintWriter shellWriter;
		private Class<?> interf;
		private Object target;
		private Map<String, CommandTypeInfo> commandTypeInfos;		
	
	public UniversialShellCommandProcessor(Object target, Class<?> interf){
		this.commandTypeInfos = new HashMap<>();
		this.interf = interf;
		this.target = target;
		this.shellReader = new BufferedReader(new InputStreamReader(System.in));
		this.shellWriter = new PrintWriter(System.out, true);		
		this.gatherCommandTypeInfoObject();
	}
	/**
	 * Generates CommandTypes based on methods which are marked with @AsCommand in this class and the AccountManager Interface
	 */
	private void gatherCommandTypeInfoObject(){
		/*
		 * generates CommandTypeInfoObjects with methods in "UniversialCommandProcessor" which are marked as @AsCommand via introspection
		 */
		Method[] shellMethods = this.getClass().getDeclaredMethods();
		Annotation[] shellAnnotations = new Annotation[shellMethods.length];
		for(int i=0;i<shellMethods.length;i++){
			shellAnnotations[i] = shellMethods[i].getDeclaredAnnotation(AsCommand.class);
			if(shellAnnotations[i] != null){
				this.commandTypeInfos.put(((AsCommand)shellAnnotations[i]).command(), new CommandType(((AsCommand)shellAnnotations[i]).command(), ((AsCommand)shellAnnotations[i]).description(), shellMethods[i], this, ((AsCommand)shellAnnotations[i]).paramTypes()));
			}
		}
		/*
		 * generates CommandTypeInfoObjects with methods in "interf" which are marked as @AsCommand via introspection
		 */
		Method[] methods = this.interf.getMethods();
		Annotation[] annotations = new Annotation[methods.length];
		for(int i=0;i<methods.length;i++){
			annotations[i] = methods[i].getDeclaredAnnotation(AsCommand.class);
			if(annotations[i] != null){
				this.commandTypeInfos.put(((AsCommand)annotations[i]).command(), new CommandType(((AsCommand)annotations[i]).command(), ((AsCommand)annotations[i]).description(), methods[i], this.target, ((AsCommand)annotations[i]).paramTypes()));
			}
		}								
	}
	/**
	 * The main loop for the shell
	 */
	public void process(){
		CommandScanner commandScanner = new CommandScanner(this.commandTypeInfos, this.shellReader);		
		while(true){
			this.shellWriter.println("Please enter a command. You can receive a list of commands by entering <help>.");
			Command command = new Command();
			try {
				commandScanner.inputLine2CommandDescriptor(command);	
				Object result = command.execute();				
				if(result != null){
					this.shellWriter.println(result);
				}
			}catch(IOException e){
				e.printStackTrace();
				this.process();
			} catch (IllegalArgumentException e){
				e.printStackTrace();
				this.process();
			} catch(RuntimeException ex){ //player did something wrong :(
				ex.printStackTrace(); //print his mistake!
				this.process();						
			} catch (IllegalAccessException e){
				e.printStackTrace();
				this.process();
			} catch (InvocationTargetException e){
				e.printStackTrace();
				this.process();
			} catch (NoSuchMethodException e){
				e.printStackTrace();
				this.process();
			}		
		}
	}
	
	@AsCommand(command ="exit", description="*exits the game", paramTypes={})
	public void exit(){ 
		System.exit(0);
	}
	
	@AsCommand(command ="help", description="*shows the list of available commands", paramTypes={})
	public void help(){
		for(CommandTypeInfo commandTypeInfo:this.commandTypeInfos.values()){
			this.shellWriter.println(commandTypeInfo.getCommand() + commandTypeInfo.getDescription());
		}
	}
}