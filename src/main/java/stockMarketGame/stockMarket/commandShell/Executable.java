package stockMarketGame.stockMarket.commandShell;

import java.lang.reflect.InvocationTargetException;

public interface Executable {
	Object execute() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException;
}