package stockMarketGame.stockMarket.commandShell;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;

import stockMarketGame.stockMarket.gameLogic.exceptions.WrongParameterException;

public class CommandScanner {

	private BufferedReader reader;
	private Map<String, CommandTypeInfo> commandTypeInfos;
	
	public CommandScanner(Map<String, CommandTypeInfo> commandTypeInfos, BufferedReader reader){		
		this.commandTypeInfos = commandTypeInfos;
		this.reader = reader;
	}
	/**
	 * Writes the commands which were read into a CommandDescriptor object
	 * @param commandDescriptor the CommandDescriptor-Object which will we modified
	 * @throws IOException Checked Exception for java 8 https://docs.oracle.com/javase/8/docs/api/java/io/IOException.html
	 */
	public void inputLine2CommandDescriptor(CommandDescriptor commandDescriptor)throws IOException{
		String[] line = this.reader.readLine().split(" ");		
		if(line.length == 0){
			return;
		}
		CommandTypeInfo commandTypeInfo = this.commandTypeInfos.get(line[0]);
		if(commandTypeInfo==null){
			throw new WrongParameterException();
		}	
		commandDescriptor.setCommandType(commandTypeInfo);//set command
		Class<?>[] paramTypes = commandTypeInfo.getParamTypes();
		Object[] params = new Object[paramTypes.length];
		if(line.length-1 != paramTypes.length){//if input parameters are too small or too large
			throw new WrongParameterException();
		}
		for(int j=0;j<paramTypes.length;j++){
			if(paramTypes[j] == int.class){
				try{
					params[j] = Integer.parseInt(line[j+1]);	
				}catch(NumberFormatException e){ // if input is not an Integer
					throw new WrongParameterException();
				}
				continue;							
			}
			params[j] = line[j+1];						
		}
		commandDescriptor.setParams(params);
	}		
}