package stockMarketGame.stockMarket.commandShell;
/**
 * Annotation which is used to mark methods which the CommandProcessor recognizes
 */
import java.lang.annotation.*;
@Inherited
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface AsCommand {
	String command();
	String description();
	Class<?>[] paramTypes();
}