package stockMarketGame.stockMarket.commandShell;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javafx.scene.control.Label;
import stockMarketGame.stockMarket.gameLogic.exceptions.NotEnoughMoneyException;
import stockMarketGame.stockMarket.gameLogic.exceptions.NullParameterException;
import stockMarketGame.stockMarket.gameLogic.exceptions.PlayerAlreadyExistsException;
import stockMarketGame.stockMarket.gameLogic.exceptions.PlayerNotFoundException;
import stockMarketGame.stockMarket.gameLogic.exceptions.ShareNotFoundException;
import stockMarketGame.stockMarket.gameLogic.exceptions.WrongParameterException;

public class UniversialGuiCommandProcessor {
		private Label returnLabel;
		private Class<?> interf;
		private Object target;
		private Map<String, CommandTypeInfo> commandTypeInfos;		
	
	public UniversialGuiCommandProcessor(Object target, Class<?> interf, Label returnLabel){
		this.commandTypeInfos = new HashMap<>();
		this.interf = interf;
		this.target = target;
		this.returnLabel = new Label();		
		this.gatherCommandTypeInfoObject();
		this.returnLabel = returnLabel;
	}
	/**
	 * Generates CommandTypes based on methods which are marked with @AsCommand in this class and the AccountManager Interface
	 */
	private void gatherCommandTypeInfoObject(){
		/*
		 * generates CommandTypeInfoObjects with methods in "UniversialCommandProcessor" which are marked as @AsCommand via introspection
		 */
		Method[] shellMethods = this.getClass().getDeclaredMethods();
		Annotation[] shellAnnotations = new Annotation[shellMethods.length];
		for(int i=0;i<shellMethods.length;i++){
			shellAnnotations[i] = shellMethods[i].getDeclaredAnnotation(AsCommand.class);
			if(shellAnnotations[i] != null){
				this.commandTypeInfos.put(((AsCommand)shellAnnotations[i]).command(), new CommandType(((AsCommand)shellAnnotations[i]).command(), ((AsCommand)shellAnnotations[i]).description(), shellMethods[i], this, ((AsCommand)shellAnnotations[i]).paramTypes()));
			}
		}
		/*
		 * generates CommandTypeInfoObjects with methods in "interf" which are marked as @AsCommand via introspection
		 */
		Method[] methods = this.interf.getMethods();
		Annotation[] annotations = new Annotation[methods.length];
		for(int i=0;i<methods.length;i++){
			annotations[i] = methods[i].getDeclaredAnnotation(AsCommand.class);
			if(annotations[i] != null){
				this.commandTypeInfos.put(((AsCommand)annotations[i]).command(), new CommandType(((AsCommand)annotations[i]).command(), ((AsCommand)annotations[i]).description(), methods[i], this.target, ((AsCommand)annotations[i]).paramTypes()));
			}
		}								
	}
	/**
	 * executes an inputString
	 * @param guiInput
	 */
	public void process(String guiInput){
		CommandWriter commandWriter = new CommandWriter(this.commandTypeInfos, guiInput);		
			Command command = new Command();
			try {
				commandWriter.inputLine2CommandDescriptor(command);	
				Object result = command.execute();				
				if(result != null){
					result.toString();
				}
			}catch(IOException e){
				e.printStackTrace();
			}catch (IllegalArgumentException e){
				e.printStackTrace();
			}catch(RuntimeException e){ //player did something wrong :(
				e.printStackTrace();
			}catch (IllegalAccessException e){
				e.printStackTrace();
			}catch (InvocationTargetException e){
				e.printStackTrace();
			}catch (NoSuchMethodException e){
				e.printStackTrace();
			}	
		}
	
	
	@AsCommand(command ="exit", description="*exits the game", paramTypes={})
	public void exit(){ 
		System.exit(0);
	}
	
	@AsCommand(command ="help", description="*shows the list of available commands", paramTypes={})
	public void help(){
		StringBuilder sBuilder = new StringBuilder();
		int counter = 0;
		for(CommandTypeInfo commandTypeInfo:this.commandTypeInfos.values()){
			sBuilder.append(commandTypeInfo.getCommand() + commandTypeInfo.getDescription());
			counter++;
			if(counter%1 == 0){
				sBuilder.append("\n");
			}
		}
		this.returnLabel.setText(sBuilder.toString());
	}
}
