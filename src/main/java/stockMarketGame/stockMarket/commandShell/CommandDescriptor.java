package stockMarketGame.stockMarket.commandShell;
/**
 * 
 * @author David
 *An object which holds the specific valies of a command
 */
public class  CommandDescriptor{

	protected CommandTypeInfo commandType;
	protected Object [] params;
	
	public CommandTypeInfo getCommandType() {
		return this.commandType;
	}

	public void setCommandType(CommandTypeInfo commandType) {
		this.commandType = commandType;
	}
	
	public Object[] getParams() {
		return this.params;
	}

	public void setParams(Object[] params) {
		this.params = params;
	}
}
