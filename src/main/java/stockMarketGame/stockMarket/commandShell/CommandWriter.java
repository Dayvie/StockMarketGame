package stockMarketGame.stockMarket.commandShell;

import java.io.IOException;
import java.util.Map;

import stockMarketGame.stockMarket.gameLogic.exceptions.WrongParameterException;
/**
 * Replaces the CommandScanner class for the GUI-Processor
 * @author David
 *
 */
public class CommandWriter {
	
	private Map<String, CommandTypeInfo> commandTypeInfos;
	private String input;
	
	public CommandWriter(Map<String, CommandTypeInfo> commandTypeInfos, String input){
		this.commandTypeInfos = commandTypeInfos;
		this.input = input;
	}
	
	public void inputLine2CommandDescriptor(CommandDescriptor commandDescriptor)throws IOException{
		String[] line = this.input.split(" ");
		if(line.length == 0){
			return;
		}
		CommandTypeInfo commandTypeInfo = this.commandTypeInfos.get(line[0]);
		if(commandTypeInfo==null){
			throw new WrongParameterException();
		}	
		commandDescriptor.setCommandType(commandTypeInfo);//set command
		Class<?>[] paramTypes = commandTypeInfo.getParamTypes();
		Object[] params = new Object[paramTypes.length];
		if(line.length-1 != paramTypes.length){//if input parameters are too small or too large
			throw new WrongParameterException();
		}
		for(int j=0;j<paramTypes.length;j++){
			if(paramTypes[j] == int.class){
				try{
					params[j] = Integer.parseInt(line[j+1]);	
				}catch(NumberFormatException e){ // if input is not an Integer
					throw new WrongParameterException();
				}
				continue;							
			}
			params[j] = line[j+1];						
		}
		commandDescriptor.setParams(params);
	}		
}