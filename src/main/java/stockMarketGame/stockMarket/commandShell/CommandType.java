package stockMarketGame.stockMarket.commandShell;
import java.lang.reflect.*;
/**
 * Holds Commands for the Commandprocessor classes
 * @author David
 *
 */
public class CommandType implements CommandTypeInfo {
	
	private String command;
	private String description;
	private Class<?>[] paramTypes;
	private Method method;
	private Object target;

	public CommandType(String command, String description, Method method, Object target, Class<?>... paramTypes){
		this.command = command;
		this.description = description;
		this.method = method;
		this.target = target;
		this.paramTypes = paramTypes;		
	}
	
	@Override
	public String getCommand() {
		return this.command;
	}

	@Override
	public String getDescription() {
		return this.description;
	}

	@Override
	public Class<?>[] getParamTypes() {
		return this.paramTypes;
	}

	@Override
	public Method getMethod() {
		return this.method;
	}

	@Override
	public Object getTarget() {
		return this.target;
	}
}
