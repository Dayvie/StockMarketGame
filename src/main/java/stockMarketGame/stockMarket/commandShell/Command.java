package stockMarketGame.stockMarket.commandShell;

import java.lang.reflect.InvocationTargetException;

import stockMarketGame.stockMarket.gameLogic.exceptions.WrongParameterException;
/**
 * 
 * @author David
 *Is a abbreviation of CommandDescriptor and therefore a Object which primarily holds values which are used for 
 *the execution of commands. Is also able to execute itself. See execute().
 */
public class Command extends CommandDescriptor implements Executable {
	@Override
	public Object execute() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{		
		if(this.commandType==null){
			throw new WrongParameterException();
		}														
		return this.commandType.getMethod().invoke(this.commandType.getTarget(), this.params);
	}
}