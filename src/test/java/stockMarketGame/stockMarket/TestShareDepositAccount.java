package stockMarketGame.stockMarket;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import stockMarketGame.stockMarket.gameLogic.assets.ShareDepositAccount;
import stockMarketGame.stockMarket.gameLogic.assets.ShareItem;
import stockMarketGame.stockMarket.gameLogic.exceptions.NullParameterException;
import stockMarketGame.stockMarket.gameLogic.exceptions.ShareNotFoundException;
import stockMarketGame.stockMarket.gameLogic.stockMarket.Share;

public class TestShareDepositAccount {

	ShareDepositAccount shareDepositAccount;
	Share share;
	Share share2;
	Share share3;
	int amount;
	int sharePrice;
	
	@Before
	public void setUp() throws Exception {
		this.shareDepositAccount = new ShareDepositAccount("PlayerName");
		this.amount = 5;
		this.sharePrice =5;
		this.share = new Share("BMW", this.sharePrice);
		this.share2 = new Share ("Audi", this.sharePrice);
		this.share3 = new Share ("DB", this.sharePrice);	
	}

	@Test
	public void buyShare() {
		this.shareDepositAccount.buyShare(this.share, this.amount);
		ShareItem shareItem = this.shareDepositAccount.getShareItems()[0];
		Assert.assertTrue(shareItem.getShare().equals(this.share)&shareItem.getAmount() == this.amount);		
	}
	/*
	 * Tests if amounts of ShareItem objects are successfully subtracted 
	 */
	@Test
	public void sellShareAmount(){
		this.shareDepositAccount.buyShare(this.share, this.amount);
		this.shareDepositAccount.sellShare(this.share, 2);
		Assert.assertTrue(this.shareDepositAccount.getShareItems()[0].getAmount() == 3);
	}
	/*
	 * Tests if ShareItem objects are successfully deleted after their amount became zero and if the total value of the share items are correctly calculated
	 */
	@Test
	public void tradeSequence(){
		Assert.assertEquals(0, this.shareDepositAccount.getValue());
		this.shareDepositAccount.buyShare(this.share, this.amount);
		Assert.assertEquals(this.share.getSharePrice()*this.amount, this.shareDepositAccount.getValue());
		this.shareDepositAccount.sellShare(this.share, this.amount);
		Assert.assertEquals(0, this.shareDepositAccount.getValue());
		this.shareDepositAccount.buyShare(this.share2, this.amount);
		this.shareDepositAccount.buyShare(this.share2, this.amount);
		this.shareDepositAccount.buyShare(this.share3, this.amount);
		Assert.assertTrue((this.share2.getSharePrice()*this.amount)*2+this.share3.getSharePrice()*this.amount == this.shareDepositAccount.getValue());
	}
	
	@Test(expected=ShareNotFoundException.class)
	public void shareNotFoundException(){
		this.shareDepositAccount.sellShare(this.share, this.amount);
	}
	
	@Test(expected=NullParameterException.class)
	public void nullConstructor(){
		ShareDepositAccount shareDepositAccount = new ShareDepositAccount(null);
	}
}