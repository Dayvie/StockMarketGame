package stockMarketGame.stockMarket;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import stockMarketGame.stockMarket.gameLogic.assets.CashAccount;
import stockMarketGame.stockMarket.gameLogic.exceptions.NotEnoughMoneyException;
import stockMarketGame.stockMarket.gameLogic.exceptions.NullParameterException;
import stockMarketGame.stockMarket.gameLogic.exceptions.TransactionException;

public class TestCashAccount {

	CashAccount cashAccount;
	long testValue;
	long resultWithdraw;
	long resultDeposit;
	
	@Before
	public void setUp()  {
		this.cashAccount = new CashAccount("Test", 1000);
		this.testValue = 5;
		this.resultWithdraw = 995;
		this.resultDeposit = 1005;
	}

	@Test
	public void deposit(){
		this.cashAccount.deposit(this.testValue);
		Assert.assertEquals(this.resultDeposit, this.cashAccount.getValue());
	}
	
	@Test
	public void withdraw(){
		this.cashAccount.withdraw(this.testValue);
		Assert.assertEquals(this.resultWithdraw, this.cashAccount.getValue());
	}
	
	@Test
	public void depositFalse(){
		this.cashAccount.deposit(this.testValue+1);
		Assert.assertNotEquals(this.resultDeposit, this.cashAccount.getValue());
	}
	
	@Test
	public void withdrawFalse(){
		this.cashAccount.withdraw(this.testValue+1);
		Assert.assertNotEquals(this.resultWithdraw, this.cashAccount.getValue());
	}
	
	@Test(expected=NotEnoughMoneyException.class)
	public void NotEnoughMoneyException(){
		this.cashAccount.withdraw(2000);
	}
	
	@Test(expected=TransactionException.class)
	public void depositTransactionException(){
		this.cashAccount.deposit(-1);
	}
	
	@Test(expected=TransactionException.class)
	public void withdrawTransactionException(){
		this.cashAccount.withdraw(-1);
	}
	
	 @Test(expected=NullParameterException.class)
	 public void nullParameterconstructor(){
		 CashAccount ca = new CashAccount(null, 1);
	 }
}
