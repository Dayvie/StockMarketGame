package stockMarketGame.stockMarket;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import stockMarketGame.stockMarket.gameLogic.assets.ShareItem;
import stockMarketGame.stockMarket.gameLogic.exceptions.EmptyShareItemException;
import stockMarketGame.stockMarket.gameLogic.exceptions.NullParameterException;
import stockMarketGame.stockMarket.gameLogic.exceptions.TransactionException;
import stockMarketGame.stockMarket.gameLogic.stockMarket.Share;

public class TestShareItem {
	
	ShareItem shareItem;
	ShareItem shareItem2;
	Share share;
	int amount;
	String playerName;
	
	@Before
	public void setUp() throws Exception {
		this.amount = 5;
		this.playerName = "playerName";
		this.share = new Share("BMW", 5);
		this.shareItem = new ShareItem(this.playerName, this.share , this.amount);		
	}

	@Test
	public void constructor(){
		this.shareItem2 = new ShareItem(this.playerName, this.share ,this.amount);
		Assert.assertTrue(this.shareItem2.getValue() == this.amount * this.share.getSharePrice() &
				this.shareItem2.getAmount() == this.amount &
				this.shareItem2.getName().equals(this.playerName));
	}

	@Test
	public void buyShare() {
		this.shareItem.buyShare(this.amount);
		Assert.assertTrue(this.shareItem.getAmount() == this.amount*2 &
				this.shareItem.getValue() == (this.share.getSharePrice()*this.amount)*2);
	}
	
	@Test
	public void sellShare() {
		this.shareItem.buyShare(this.amount);
		this.shareItem.sellShare(this.amount);
		Assert.assertTrue(this.shareItem.getAmount() == this.amount &
				this.shareItem.getValue() == this.shareItem.getShare().getSharePrice()*this.amount);		
	}
	
	@Test(expected=TransactionException.class)
	public void buyShareBadParameter() {
		this.shareItem.buyShare(0);
	}
	
	@Test(expected=TransactionException.class)
	public void sellShareBadParameter() {
		this.shareItem.sellShare(0);
	}
	
	@Test(expected=NullParameterException.class)
	public void nullParameterconstructor(){
		ShareItem shareItem3 = new ShareItem(null, this.share, 2);
	}
	
	@Test(expected=EmptyShareItemException.class)
	public void emptyShareItem(){
		ShareItem shareItem3 = new ShareItem(this.playerName, this.share, 0);
	}
}